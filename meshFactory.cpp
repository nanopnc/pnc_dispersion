#include "meshFactory.hpp"

/** Kalvo Mesh Generator class constructor
 * 
 * @author Panu Lappalainen
 * @version 211005
 * 
 * @param settings reference contains parsed and default parameters
 */
MeshFactory::MeshFactory(Settings& settings)
{
    setAttributes(settings); // Set the settings properties as class attributes
    solveSizes(); // Calculate the dimensions and required element counts
    initializeCounts(); // Initialize and calculate count attributes

    double vertices[nVert][3]; // Initialize an empty matrix for vertices
    solveVertices(vertices); // Calculate vertices

    int nHBQ = (nMatElems.size()+1)*xCount*yCount; // Number of horizontal quads
    int horizontalBoundaryQuads[nHBQ][4]; // For top/bottom/interlayer quads
    c->p(E + "Space reserved for horizontal boundary quads: " 
        + to_string(nHBQ));

    bool blueprint[xCount-1][yCount-1] = {0}; // Empty matrix for shape blueprint    
    int hexahedrons[nElem][8]; // Empty matrix for hexahedron elements (cubes)
    nElem = solveHexahedrons(hexahedrons, horizontalBoundaryQuads, 
        (bool*)blueprint); // Solving the cubes and horizontal quads

    int intBoundaries[nBorderQuad][4]; // Empty matrix for internal boundaries
    solveInternalBoundaries(intBoundaries, hexahedrons, (bool*)blueprint); 

    // Print the blueprint on console
    if (myid == 0 && p>=mods) printShape((bool*)blueprint); 

    // Create (1) 3D mesh with (2) "nVert" vertices, (3) "nElem" cube elements
    // and (4) "nBorderQuad+nHorQuad+nIntQuad" boundary quads, in (5) 3D space.
    mesh = new Mesh(3, nVert, nElem, nBorderQuad+nHorQuad+nIntQuad, 3);
    makeMesh(*mesh, vertices, hexahedrons, horizontalBoundaryQuads, 
        intBoundaries);

    // Create coordinate function
    fec = new L2_FECollection(1, 3, BasisType::GaussLobatto);
    // H1_FECollection *fec = new H1_FECollection(1,3);
    fes = new FiniteElementSpace(mesh, fec, 3, Ordering::byVDIM);
    GridFunction *coords = new GridFunction(fes);	

    for (int i = 0; i < mesh->GetNE(); i++) // For each element (hex)
    {
        const int h = 3*8*i; // Initial 3D hex (8-point) vertex iterator
        Array<int> verts; // Initialize hex vertex array
        mesh->GetElementVertices(i, verts); // Get vertex data into verts
        
        // Construct the      6-----7     
        // hex coordinates   /|    /| 
        // with nodes from  4-----5 |
        // bottom to top    | 2---|-3 
        // zig-zag-wise     |/    |/ 
        // in this order    0-----1

        // Rearrange hex data to node coordinate form: 01234567->01324576 
        int temp = verts[3]; 
        verts[3] = verts[2]; verts[2] = temp; // Swap 3 and 2
        temp = verts[7];
        verts[7] = verts[6]; verts[6] = temp; // Swap 7 and 6

        // Coordinates of the "initial" bottom left corner vertex
        const double *initC = mesh->GetVertex(verts[0]);
        
        for (int v = 0; v < 8; v++) // For each vertex in the hex
        {
            // Local copies
            double* c = mesh->GetVertex(verts[v]);
            double xCoord = c[0]; double yCoord = c[1];
            
            if (periodic) 
            {
                // Do not loop over edges, but keep going
                if (xCoord < initC[0]-hexWidth) 
                    xCoord = (hexWidth*(xCount-1))/2;
                if (yCoord < initC[1]-hexBreadth) 
                    yCoord = (hexBreadth*(yCount-1))/2;
            }

            // Set X, Y and Z
            (*coords)[v*3+h+0] =  xCoord; 
            (*coords)[v*3+h+1] =  yCoord;
            (*coords)[v*3+h+2] =  c[2];
        }
    }

    // Set the new coordinates and transfer ownership to the mesh
    mesh->NewNodes(*coords, true);

    { // Write the mesh to a file
        ofstream ofs("pnc.mesh");
        mesh->Print(ofs);
        ofs.close();
    }

    c->p("Serial mesh successfully created");
}

/** Class destructor */
MeshFactory::~MeshFactory()
{
    delete mesh; // Delete the mesh
    delete fec; // Delete the FiniteElementCollection
    delete fes; // Delete the FiniteElementSpace
}

/** Solves the dimensions of the lattice and its component hexahedrons
 *
 * @param size address of the length a dimension of the lattice,
 *   original value changed if not defined
 * @param count address of the number of vertices in a dimension of the lattice,
 *   original value changed if not defined
 * @param defaultSize length of a dimension of the lattice given to undefined 
 *  size
 * @param preferredHexSize used to define size or count when the other is not 
 *   defined, also a hard limit below which the hexahedron size cannot be
 *   automatically scaled
 * @return length of a dimension of the component hexahedron
 */
double MeshFactory::SolveHexSide(double& size, int& count, double defaultSize, 
    double preferredHexSize)
{
    if (size <= 0) // Size not defined
    {
        if (count <= 1) // Neither size nor count defined
        { 
            // Solving for both size and count based on defaultSize
            size = defaultSize;
            count = size / preferredHexSize + 1;
        }
        else // Only size not defined, solving for size
            size = preferredHexSize * (count-1);
    }
    else if (count <= 1) // Only count not defined, solving for count
    {
        // The quotient is saved in int, essentially "rounding down" the count.
        // This causes the created cubes to be "stretched" to right size in
        // the lattice to make up the required shape.
        count = size / preferredHexSize + 1; 
    }
    return size / (count-1); 
}

/** Prints the blueprint of the lattice
 * 
 * @param blueprint pointer to the 2D boolean array of the elements
 */
void MeshFactory::printShape(bool * blueprint)
{
    string formatting = "\033[2;34m"; // Set blue format
    cout << formatting << "Blueprint (" << (hCount-1) << " layers):" << endl;
    for (int y = 0; y < yCount-1; y+=2) // For each pair of rows
    {
        cout << formatting;
        for (int x = 0; x < xCount-1; x++) // For each column
        {
            bool upper = *(blueprint+y*(xCount-1)+x); // Cube boolean at (x, y)

            // If Y is odd, the last "pair of rows" only has 1 real row
            bool lower = false; // Assume "fake row"
            if (!(y == yCount-2 && (yCount-1) % 2 == 1)) // Not "fake row"
                lower = *(blueprint+(y+1)*(xCount-1)+x); // Cube bool @(x, y+1)

            // Cube up and down
            if (upper && lower) cout << "█"; 
            // Cube down only
            else if (!upper && lower) cout << "▄";
            // Cube up only
            else if (upper && !lower) cout << "▀";
            // No cubes (hole)
            else cout << " "; 
        }
        cout << "\033[0m" << endl; // De-format back to regular white
    }
}

/** Solves the boundaries surrounding the hole
 * @param intBoundaries matrix of the internal boundaries to be filled with
 *  vertex information
 * @param hexahedrons matrix of the cubes' vertex information
 * @param blueprint pointer to the 2D boolean array of the elements
 */
void MeshFactory::solveInternalBoundaries(int intBoundaries[][4], 
    int hexahedrons[][8], bool * blueprint)
{
    if (!makeBoundaries || r < hexWidth/2) return; // Do not make boundaries

    // Array for hexahedron rows in X-direction
    int xHexes[yCount-1] = {0}; 
    for (int y = 0; y < yCount-1; y++)
    {
        for (int x = 0; x < xCount-1; x++)
            if (*(blueprint+y*(xCount-1)+x)) xHexes[y]++;
    }

    // Array for hexahedron columns in Y-direction
    int yHexes[xCount-1] = {0};
    for (int x = 0; x < xCount-1; x++)
    {
        for (int y = 0; y < yCount-1; y++)
            if (*(blueprint+y*(xCount-1)+x))yHexes[x]++;
    }

    // X-direction:
    {
        for (int i = 1; i < nElem-1; i++)
        {
            // Check for exterior border
            if (hexahedrons[i][0] % xCount == 0) continue;

            // If the first vertices of consequetive cubes differ, they are not
            // neighbours, thus marking the location of the hole
            if (hexahedrons[i][0]-1 != hexahedrons[i-1][0]) 
            {
                // Create a quad down on the spot
                intBoundaries[nIntQuad][0] = hexahedrons[i][0];
                intBoundaries[nIntQuad][1] = hexahedrons[i][3];
                intBoundaries[nIntQuad][2] = hexahedrons[i][3]+xCount*yCount;
                intBoundaries[nIntQuad][3] = hexahedrons[i][0]+xCount*yCount;
                nIntQuad++; // Increase the quad number

                // Create a quad down on the opposite side of the hole
                intBoundaries[nIntQuad][0] = hexahedrons[i-1][1];
                intBoundaries[nIntQuad][1] = hexahedrons[i-1][2];
                intBoundaries[nIntQuad][2] = hexahedrons[i-1][2]+xCount*yCount;
                intBoundaries[nIntQuad][3] = hexahedrons[i-1][1]+xCount*yCount;
                nIntQuad++; // Increase the quad number
            }
        }
    }       

    // Y-direction:
    {
        int iFHL = 0; // Index of first hex on the line
        int nIYQ = 0; // Number of internal Y-directional quad chunks created
        int nEPL = nElem / (hCount-1); // Number of elements per layer
        for (int i = 0; i < yCount/2; i++)
        {
            int hOL = xHexes[i]; // Hexes on the line
            if (xHexes[i+1] < (xCount-1)) 
            {
                // Number of required Y-squads to fill the hole edges
                int nRS = (xCount-1)-xHexes[i+1]  - (nIYQ); 

                // For each required squad
                for (int j = 0; j < nRS; j++)
                {
                    // For all layers (Z-direction)
                    for (int h=0; h<hCount-1; h++)
                    {
                        // Index of the hex
                        int nHex = iFHL + xHexes[i+1]/2 + j; 

                        // Create a quad down on the spot
                        intBoundaries[nIntQuad][0] = 
                            hexahedrons[nHex][2]+xCount*yCount*h;
                        intBoundaries[nIntQuad][1] = 
                            hexahedrons[nHex][3]+xCount*yCount*h;
                        intBoundaries[nIntQuad][2] =    
                            hexahedrons[nHex][3]+xCount*yCount*(h+1);
                        intBoundaries[nIntQuad][3] = 
                            hexahedrons[nHex][2]+xCount*yCount*(h+1);
                        nIntQuad++; // Increase the quad number

                        // Create a quad down on the opposite side of the hole
                        intBoundaries[nIntQuad][0] = 
                            hexahedrons[nEPL-1-nHex][0]+xCount*yCount*h;
                        intBoundaries[nIntQuad][1] = 
                            hexahedrons[nEPL-1-nHex][1]+xCount*yCount*h;
                        intBoundaries[nIntQuad][2] = 
                            hexahedrons[nEPL-1-nHex][1]+xCount*yCount*(h+1);
                        intBoundaries[nIntQuad][3] = 
                            hexahedrons[nEPL-1-nHex][0]+xCount*yCount*(h+1);
                        nIntQuad++; // Increase the quad number

                    }
                    nIYQ++; // Increase number of quad chunks created
                }
            }
            iFHL +=  hOL; // Increase the index of first hex on the line
        }
    }    
    c->p(E + "Number of internal quads created: " + to_string(nIntQuad), all);
}

/** Sets the class attributes
 * 
 * @param settings reference contains the settings
 */ 
void MeshFactory::setAttributes(Settings& settings)
{
    // Print level
    p=settings.getPrintLevel();
    c=settings.getPrintingHandler();
    myid = c->getMyid();  

    // Dimensions
    xWidth = settings.getLatticeWidth();
    yBreadth = settings.getLatticeBreadth();
    heights = settings.getThicknesses();
    { // Height of the entire lattice is the sum of layer heights
        const int LEN = (int) heights.size();
        for (int i = 0; i < LEN; i++) thickness += heights[i];    
    }

    // Vertex counts (add 1 to each dimension)
    vector<int> elements = settings.getNumberOfElements();
    xCount = elements[0]+1;
    yCount = elements[1]+1;
    hCount = elements[2]+1;

    // Radius of the hole
    r1 = settings.getRadius();
    r2 = settings.getRadius2();
    r = (r1+r2)/2;
    preferredHexSize = settings.getComponentHexPreferredSize();

    // Visualization options
    makeBoundaries = settings.getBoundaryBoolean();
    visualization = settings.getVisualization();
    snapping = settings.getSnapping();
    periodic = settings.getPeriodicity();
    maxRand = settings.getRandomness();

    // Set material properties in settings class for CLEEPS
    Material m(c);
    vector<string> materialList = settings.getListOfMaterials();
    const int LEN = (int) materialList.size();
    for (int i = 0; i < LEN; i++)
    {
        m.setAllProperties(materialList[i]);
        settings.pushbackLambda(m.getLambda());
        settings.pushbackMu(m.getMu());
        settings.pushbackRho(m.getRho());
    }
}

/** Fills the matrix given with the coordinates calculated for the vertices.
 * 
 * @param vertices matrix to be filled with positional information
 * @returns the number of vertices solved
 */
int MeshFactory::solveVertices(double vertices[][3])
{
    vector<double> hexHeights; // For tracking all element layers' heights
    hexHeights.push_back(0); // First height is the floor, 0

    const int LEN = (int) heights.size();
    for (int m = 0; m < LEN; m++)
    {
        double currentHexHeight = heights[m]/nMatElems[m];
        for (int n = 0; n < nMatElems[m]; n++) 
            hexHeights.push_back(currentHexHeight);
    }

    // Print hexahedron dimensions
    if (myid == 0 && p >= mods)
    {
        string heightsString = "(";
        const int LEN2 = (int) hexHeights.size();
        for (int i = 1; i < LEN2; i++)
        {
            heightsString += to_string(hexHeights[i]);
            if (i < LEN2-1) heightsString += "/";
        }
        heightsString += ")";
        cout << "Hexahedron dimensions: " << hexWidth << "x" << hexBreadth 
            << "x" << heightsString << endl;
    }

    // Corner positions
    double cornerX = (xCount-1)*hexWidth/2;
    double cornerY = (yCount-1)*hexBreadth/2;

    int vertexIterator = 0; // Integer for tracking the number of vertices
    double cumulativeHexHeight = 0; // For tracking the ongoing height
    for (int h = 0; h < hCount; h++) // Vertical dimension
    {
        // Add the current layer's hex height to the cumulative hex height
        cumulativeHexHeight += hexHeights[h];

        for (int y = 0; y < yCount; y++) // Y-direction (horizontal)
        {
            for (int x = 0; x < xCount; x++) // X-direction (horizontal)
            {
                vertices[vertexIterator][0] = x*hexWidth - cornerX;
                vertices[vertexIterator][1] = y*hexBreadth - cornerY;
                vertices[vertexIterator][2] = cumulativeHexHeight;
                vertexIterator++; // Increase the vertex number
            }               
        }           
    }             
    c->p(E + "Number of vertices created: " + to_string(vertexIterator), all);
    return vertexIterator;
}

/** Fills the given matrix with the indices of the vertices calculated to 
 *  form the 3-dimentional hexahedron elements (cubes). 
 * 
 * @param hexahedrons matrix for hexes to be filled with vertex information
 * @param horizontalBoundaryQuads matrix for top, bottom and interlayer quads 
 *  to be filled with vertex information
 * @param blueprint pointer to the 2D boolean array of the elements
 * @returns the number of hexahedrons solved
 */
int MeshFactory::solveHexahedrons(int hexahedrons[][8], 
    int horizontalBoundaryQuads[][4], bool * blueprint)
{
    // Corner locations
    double cornerX = (xCount-1)*hexWidth*0.5;
    double cornerY = (yCount-1)*hexBreadth*0.5;

    // Cumulative number of elements per layer of material
    const int LEN = (int) nMatElems.size();
    int cumulativeMatElems[LEN] = {0};
    {
        int sum = 0; // For tracking the sum of numbers of elements
        for (int m = 0; m < LEN; m++)
        {
            sum += nMatElems[m]; // Add the number to the sum
            cumulativeMatElems[m] = sum; // Current sum is cumulative 
        }
    }

    int elementIterator = 0; // For tracking the number of vertices
    for (int h = 0; h <= hCount-1; h++) // Vertical dimension
    {
        for (int y = 0; y < yCount-1; y++) // Y-direction (horizontal)
        {
            for (int x = 0; x < xCount-1; x++) // X-direction (horizontal)
            {
                double xPos = x*hexWidth-cornerX;
                double yPos = y*hexBreadth-cornerY;
                bool outside = false; // Cube outside the hole

                // Not drawing cubes with ALL vertices inside
                if (sqrt(xPos*xPos + yPos*yPos) >= r) outside = true;
                xPos += hexWidth; 
                if (sqrt(xPos*xPos + yPos*yPos) >= r) outside = true;
                yPos += hexBreadth;
                if (sqrt(xPos*xPos + yPos*yPos) >= r) outside = true;
                xPos -= hexWidth;
                if (sqrt(xPos*xPos + yPos*yPos) >= r) outside = true;

                // Save cube/hole information in blueprint
                *(blueprint+y*(xCount-1)+x) = outside;

                // Create elements only for area outside the hole
                if (outside)
                {
                    // Find out if at interlayer boundary
                    bool materialBoundary = false;
                    const int LEN = nMatElems.size()-1;
                    for (int m = 0; m < LEN; m++)
                    {
                        if (h == cumulativeMatElems[m])
                        {
                            materialBoundary = true;
                            break;
                        }
                    }

                    // For fusing the opposite edges with each other
                    int xFuse = 0; int yFuse = 0;
                    if (periodic)
                    {
                        // "Fuse" the last boundary   0__1__2  0
                        // with the first boundary    |__|__|::|
                        if (x == xCount-2) xFuse = -(xCount-1);
                        if (y == yCount-2) yFuse = -xCount*(yCount-1);
                    }

                    // Bottom, top or interlayer boundary quad
                    if (makeBoundaries && (h == 0 || h == hCount-1 
                        || materialBoundary)) 
                    {
                        horizontalBoundaryQuads[nHorQuad][0] =
                            x+y*xCount+h*yCount*xCount;
                        horizontalBoundaryQuads[nHorQuad][1] = xFuse+
                            x+y*xCount+h*yCount*xCount+1;
                        horizontalBoundaryQuads[nHorQuad][2] = xFuse+yFuse+
                            x+y*xCount+h*yCount*xCount+1+xCount;
                        horizontalBoundaryQuads[nHorQuad][3] = yFuse+
                            x+y*xCount+h*yCount*xCount+xCount;
                        nHorQuad++; // Increase the quad number
                    }
                    if (h == hCount-1) continue; // No hexes above top level

                    // Construct the      7-----6     
                    // hexahedron with   /|    /|
                    // vertices from    4-----5 |
                    // bottom to top    | 3---|-2
                    // anti-clockwise   |/    |/ 
                    // in this order    0-----1
                    {
                        hexahedrons[elementIterator][0] = 
                            x+y*xCount+h*yCount*xCount;
                        hexahedrons[elementIterator][1] = xFuse+
                            x+y*xCount+h*yCount*xCount+1;
                        hexahedrons[elementIterator][2] = xFuse+yFuse+
                            x+y*xCount+h*yCount*xCount+1+xCount;
                        hexahedrons[elementIterator][3] = yFuse+
                            x+y*xCount+h*yCount*xCount+xCount;
                        hexahedrons[elementIterator][4] = 
                            x+y*xCount+h*yCount*xCount+xCount*yCount;
                        hexahedrons[elementIterator][5] = xFuse+
                            x+y*xCount+h*yCount*xCount+1+xCount*yCount;
                        hexahedrons[elementIterator][6] = xFuse+yFuse+
                            x+y*xCount+h*yCount*xCount+1+xCount+xCount*yCount;
                        hexahedrons[elementIterator][7] = yFuse+
                            x+y*xCount+h*yCount*xCount+xCount+xCount*yCount;
                    }

                    elementIterator++; // Increase the element number
                }
            }               
        }           
    }

    if (myid == 0 && p >= all) // Prints successfull creation counts
    { 
        cout << "Number of hexahedrons created: " << elementIterator << endl;
        cout << "Number of horizontal quads created: " << nHorQuad << endl;
    }
    return elementIterator;
}

/** Solves the size of a single hexahedron and, if needed, also the size of
 *  the lattice, by calling the fucntion SolveHexSide. Also prints dimensions.
 */
void MeshFactory::solveSizes()
{
    // Size of the hexahedron in X-direction
    hexWidth = SolveHexSide(xWidth, xCount, 100, preferredHexSize);
    hexBreadth = hexWidth;
    if (yBreadth <= 0)
    {
        // If the Y values are unchanged, make the base a square
        if (yCount <= 1 || yCount == xCount)
        {
            yCount = xCount;
            yBreadth = xWidth;
            c->p(E + "Y values unchanged, " +
                    "\033[1;37mmade the base a square\033[0m", all);
        }
    }
    else // Size of the hexahedron in Y-direction
        hexBreadth = SolveHexSide(yBreadth, yCount, 100, preferredHexSize); 
    // Size of the hexahedron in Z-direction
    hexHeight = SolveHexSide(thickness, hCount, 50, preferredHexSize);
    if (heights[0] <= 0) heights[0] = thickness; 

    int elementsLeft = hCount-1; // For tracking how many elements layers are left
    const int LEN = heights.size();
    for (int i = 0; i < LEN - 1; i++) // For each material except last
    {
        double heightShare = heights[i] / thickness; // Portion of total height
        // Count and add to vector the number of required layers by the material
        nMatElems.push_back(round((hCount-1) * heightShare));
        elementsLeft -= nMatElems[i]; // Reduce the number of elements left
    }
    nMatElems.push_back(elementsLeft); // Last material layer gets the remaining

    // Make sure every layer of material has at least one layer of elements
    for (int i = 0; i < LEN; i++)
    {
        if (nMatElems[i] == 0 && heights[i] > 0)
        {
            for (int j = 0; j < LEN; j++)
                if (nMatElems[j] > 1)
                {
                    nMatElems[j] -= 1;
                    nMatElems[i] += 1;
                    c->p("Layer " + to_string(i+1) +
                        " had no elements, stole one from layer " 
                        + to_string(j+1), warnings);
                    break;
                }
        }
    }

    if (myid == 0 && p >= mods) // Print dimension informartion
    {
        string hCounts = ""; string zHeights = "";
        int n = nMatElems.size();
        for (int i = 0; i < n; i++)
        {
            hCounts += to_string(nMatElems[i]);
            zHeights += to_string(heights[i]);
            if (i < n-1) 
            {
                hCounts += "+"; zHeights += "+";
            }
        }

        if (p >= all)
            cout << "Lattice dimensions: " << xWidth << "x" << yBreadth << "x(" 
                << zHeights << ")" << endl;

        cout << "Elemental layout: " << xCount-1 << "x" << yCount-1 << "x(" 
            << hCounts << ")" << endl;
    }
}

/** Sends the solution by socket to a GLVis server for visualization
 * 
 * @param mesh reference to the mesh generated
 * @param x GridFunction reference to the solution
 */
void MeshFactory::visualize(Mesh& mesh, GridFunction& x)
{
    char vishost[] = "localhost";
    int  visport   = 19916;
    socketstream sol_sock(vishost, visport);
    sol_sock.precision(8);
    string picName = "holeMeshR" + to_string(r) + ".png";
    sol_sock << "solution\n" << mesh << x << "keys m screenshot " << picName 
    // << " keys q" 
    << flush;    
}

/** Saves the refined mesh and the solution. This output can be viewed
 *  later using GLVis.
 * 
 * @param mesh reference to the mesh generated
 * @param x reference to the solution
 */
void MeshFactory::saveSolution(Mesh *mesh, GridFunction& x)
{
    ofstream mesh_ofs("pnc_hole.mesh");
    mesh_ofs.precision(8);
    mesh->Print(mesh_ofs);
    ofstream sol_ofs("sol3.gf");
    sol_ofs.precision(8); 
    x.Save(sol_ofs);
}

/** Creates and adds border boundary quads with attributes 1-4 and adds 
 *  top and bottom boundary quads with attribute 5 to the mesh. Interlayer
 *  boundaries get an attribute greater than previousAttribute.
 * 
 * @param mesh reference to the mesh
 * @param horizontalBoundaryQuads matrix of vertex information of the top and 
 *  bottom boundary quads calculated beforehand
 * @param intBoundaries matrix of the vertex information of the internal 
 *  boundary quads calculated beforehand
 * @param previousAttribute used by last element
 */
void MeshFactory::makeBoundaryQuads(Mesh& mesh, 
    int horizontalBoundaryQuads[][4], int intBoundaries[][4], 
    int previousAttribute)
{
    const int TOP_BOTTOM_ATTRIBUTE = 5; // For top/bottom boundaries
    int nEPL = nElem / (hCount-1); // Number of elements per layer

    // Add boundary quads into the mesh
    int quadIterator = 0; // Integer for tracking the number of quads
    if (makeBoundaries)
    {
        c->p(E + "Creating/adding boundaries...", all);
        for (int h = 0; h < hCount-1; h++) // Vertical dimension
        {
            int fuse = 0;
            if (periodic) fuse = -1;

            // Create border    3     
            // boundary       .---.    
            // attributes   2 | o | 4   
            // in this order  '---'     
            // (top view)       1       

            for (int y = 0; y < yCount-1+fuse; y++) // Y-direction (horizontal)
            {
                // Left boundary = 2
                const int quadLeft[4] = {
                    yCount*xCount*h+y*xCount, // 0
                    yCount*xCount*h+(y+1)*xCount, // 1
                    yCount*xCount*(h+1)+(y+1)*xCount, // 2
                    yCount*xCount*(h+1)+y*xCount // 3
                };  
                mesh.AddBdrQuad(quadLeft, 2);
                quadIterator++; // Increase the quad number      

                // Right boundary = 4
                if (periodic) // B2 in reverse order
                {
                    const int quadRight[4] = {quadLeft[2], quadLeft[1], 
                                              quadLeft[0], quadLeft[3]};
                    mesh.AddBdrQuad(quadRight, 4);   
                }
                else
                {
                    const int quadRight[4] = {
                        xCount-1+ yCount*xCount*h+y*xCount,
                        xCount-1+ yCount*xCount*h+(y+1)*xCount,
                        xCount-1+ yCount*xCount*(h+1)+(y+1)*xCount,
                        xCount-1+ yCount*xCount*(h+1)+y*xCount
                    };  
                    mesh.AddBdrQuad(quadRight, 4);                     
                }
                
                quadIterator++; // Increase the quad number          
            }
            if (periodic) // Fused Y-boundaries
            {
                // Fused left boundary = 2
                const int quadLeft[4] = {
                    yCount*xCount*h+(yCount-2)*xCount, // 0
                    yCount*xCount*h+(0)*xCount, // 1
                    yCount*xCount*(h+1)+(0)*xCount, // 2
                    yCount*xCount*(h+1)+(yCount-2)*xCount // 3
                };  
                mesh.AddBdrQuad(quadLeft, 2);
                quadIterator++; // Increase the quad number

                // Fused right boundary = 4
                const int quadRight[4] = {quadLeft[2], quadLeft[1], 
                                          quadLeft[0], quadLeft[3]};
                mesh.AddBdrQuad(quadRight, 4);   
                quadIterator++; // Increase the quad number     
            }      
            for (int x = 0; x < xCount-1+fuse; x++) // X-direction (horizontal)
            {
                // Front boundary = 1
                const int quadFront[4] = {
                    yCount * xCount * h+x, // 0
                    yCount * xCount * h+x+1, // 1
                    yCount * xCount * (h+1)+x+1, // 2
                    yCount * xCount * (h+1)+x, // 3
                };  
                mesh.AddBdrQuad(quadFront, 1);
                quadIterator++; // Increase the quad number       

                // Back boundary = 3
                if (periodic) // B1 in reverse order
                {
                    const int quadRear[4] = {quadFront[2], quadFront[1], 
                                             quadFront[0], quadFront[3]};
                    mesh.AddBdrQuad(quadRear, 3);
                }
                else
                {
                    const int quadRear[4] = {
                        xCount*(yCount-1) + yCount*xCount*h+x,
                        xCount*(yCount-1) + yCount*xCount*h+x+1,
                        xCount*(yCount-1) + yCount*xCount*(h+1)+x+1,
                        xCount*(yCount-1) + yCount*xCount*(h+1)+x,
                    };  
                    mesh.AddBdrQuad(quadRear, 3);
                }                
                quadIterator++; // Increase the quad number           
            }
            if (periodic) // Fused X-boundaries
            {
                // Fused front boundary = 1
                const int quadFront[4] = {
                    yCount * xCount * h+(xCount-2), // 0
                    yCount * xCount * h+0, // 1
                    yCount * xCount * (h+1)+0, // 2
                    yCount * xCount * (h+1)+(xCount-2), // 3
                };  
                mesh.AddBdrQuad(quadFront, 1);
                quadIterator++; // Increase the quad number

                // Fused rear boundary = 3
                const int quadRear[4] = {quadFront[2], quadFront[1], 
                                         quadFront[0], quadFront[3]};
                mesh.AddBdrQuad(quadRear, 3);
                quadIterator++; // Increase the quad number   
            }       
        }

        // Top/bottom boundaries = 5
        int boundaryAttribute = TOP_BOTTOM_ATTRIBUTE; // Starting from bottom
        previousAttribute = boundaryAttribute;
        string interlayerMessage = "Interlayer boundary attributes: ";
        for (int i = 0; i < nHorQuad; i++) // For each horizontal quad
        {
            // Ending in top boundary
            if (i == nHorQuad-nEPL) boundaryAttribute = TOP_BOTTOM_ATTRIBUTE;
            else if (i % nEPL == 0 && i != 0) // Layer change
            {
                previousAttribute++; // Change to next attribute
                boundaryAttribute = previousAttribute;
                interlayerMessage += to_string(previousAttribute) + " ";
            }
            mesh.AddBdrQuad(horizontalBoundaryQuads[i], boundaryAttribute);
        }
        c->p(interlayerMessage, all);
    }

    for (int i = 0; i < nIntQuad; i++) // For each internal boundary quad
    {
        mesh.AddBdrQuad(intBoundaries[i], TOP_BOTTOM_ATTRIBUTE);
    }

    c->p(E + "Number of border boundary quads created: " +
        to_string(quadIterator), all);
}

/** Initializes and calculates the numbers of vertices, hexahedrons and
 *  border boundary quads to allocate space for.
 */
void MeshFactory::initializeCounts()
{
    // Total number of vertices
    nVert = xCount*yCount*hCount; 
    c->p(E + "Pre-calculated number of vertices: " + to_string(nVert), all);

    // Maximum number of elements to allocate memory for
    nElem = (xCount-1)*(yCount-1)*(hCount-1); 
    c->p(E + "Space reserved for hexahedrons: " + to_string(nElem), all);

    // Total number of boundary quads
    nBorderQuad = 0;
    if (makeBoundaries) 
        nBorderQuad =  (2*(xCount-1) + 2*(yCount-1))*(hCount-1);
    c->p(E + "Pre-calculated number of border boundary quads: " + 
        to_string(nBorderQuad), all);
}

/** Performs cleanup for the mesh required to avoid segmentation faults etc
 * 
 * @param mesh reference to the mesh
 */
void MeshFactory::cleanupMesh(Mesh& mesh)
{
    // Remove the leftover vertices from the hole area and (if periodic)
    // from the edges. Renumbers vertices to continuous numbering.
    c->p(E + "Removing unused vertices...");
    mesh.RemoveUnusedVertices();
}

/** Makes the mesh
 * 
 * @param mesh reference to the mesh being made
 * @param vertices array of the positional information of the vertices
 * @param hexahedrons array of the vertex information of the cubes
 * @param horizontalBoundaryQuads array of the vertex information of the top 
 *  and bottom boundary quads
 * @param intBoundaries array of the vertex information of the internal 
 *  boundary quads
 */
void MeshFactory::makeMesh(Mesh& mesh, double vertices[][3], 
    int hexahedrons[][8], int horizontalBoundaryQuads[][4], 
    int intBoundaries[][4])
{
    // Shift the vertices to "round" the hole and balance the remainders
    if (snapping) mapSnap(vertices); 

    // Add vertices into the mesh
    c->p(E + "Adding vertices...");
    for (int v = 0; v < nVert; v++) mesh.AddVertex(vertices[v]);

    vector<int> hexAttributes; // For tracking the attributes of the layers
    int currentAttribute = 1; // Material attribute of current layer

    const int LEN = (int) heights.size();
    for (int m = 0; m < LEN; m++) // For each material
    {
        for (int n = 0; n < nMatElems[m]; n++) // For each layer of the material
            hexAttributes.push_back(currentAttribute); // Layer's attribute
        currentAttribute++; // Next material's attribute
    }

    // Add hexahedron elements into the mesh
    c->p(E + "Adding hexahedrons...");
    string attributeMessage = "Element layer attributes: ";
    int attributeIndex = -1; // For tracking the index of hexAttributes
    int nEPL = nElem / (hCount-1); // Number of elements per layer
    for (int h = 0; h < nElem; h++ ) // For each element
    {
        // Change to next attribute at layer change
        if (h % (nEPL) == 0) 
        {
            attributeIndex++;
            attributeMessage += to_string(hexAttributes[attributeIndex]) + " ";
        }
        // Add hexahedron with the corresponding attribute from hexAttributes
        mesh.AddHex(hexahedrons[h], hexAttributes[attributeIndex]);
    }
    c->p(attributeMessage); // Print usage

    // Add boundary quads into the mesh
    makeBoundaryQuads(mesh, horizontalBoundaryQuads, intBoundaries, 
        hexAttributes[hexAttributes.size()-1]);

    cleanupMesh(mesh); // Perform mesh cleanup
}

/** Checks if the given quadrilateral is convex or concave
 * 
 * @param q array of coordinate information of the quadrilateral
 * @param convexities array of the signs of the cross products of the adjacent
 *  sides of each corner vertex as booleans, to be altered
 * @returns whether the quad is convex
 */
bool isConvex(double q[4][2], bool convexities[4])
{
    bool convex = true; // Assume the quad is convex
    bool sign = false; // Initial guess for sign of first cp
    int n = 4; // Number of corners

    for(int i = 0; i < n; i++) // For each corner point
    {
        // Adjacent left side
        double dx1 = q[(i+1)%n][0]- q[(i)%n][0];
        double dy1 = q[(i+1)%n][1]- q[(i)%n][1];
                
        // Adjacent right side
        double dx2 = q[(i+2)%n][0]- q[(i+1)%n][0];
        double dy2 = q[(i+2)%n][1]- q[(i+1)%n][1];

        // Z-directional cross product of the sides
        double zCrossProduct = dx1 * dy2 - dy1 * dx2;

        bool zCp = zCrossProduct > 0; // Sign of the cross product as boolean
        if (i == 0) sign = zCp; // Flip the initial guess of cp sign
        else if (sign != (zCp)) convex = false; // Different sign means concave
        convexities[(i+1)%n] = zCp; // Save sign in array
    }
    return convex;
}

/** Shifts the vertex positions at the edges of the hole into a regular polygon 
 *  to better reflect the circular shape of the hole. Balances the remainder
 *  vertices to gradually decreasing shifting.
 * 
 * @param vertices array of the positional information of the vertices
 */
void MeshFactory::mapSnap(double vertices[][3])
{
    // Set border vertices as maximums, these should not be shifted
    const double MAX_X = hexWidth*(xCount-1)/2; 
    const double MAX_Y = hexBreadth*(yCount-1)/2; 

    int nodesShifted = 0; // For tracking the number of shifted nodes
    srand(1); // Initialize rand
    int nVPL = nVert/hCount;
    double divisor = 1000.0; // Essentially makes the integer into double
    if (hexWidth/4 <= maxRand/divisor)
    {
        c->p(string("Maximum lattice point random variation" 
                "is greater than element size!"), warnings);
    }

    for (int i = 0; i < nVPL; i++)
    {
        // Vertex coordinates
        double x = vertices[i][0]; 
        double y = vertices[i][1];       
            
        double mag = std::sqrt(x*x+y*y); // Distance from center (circular)

        if (mag <= r) // Vertices within the hole
        {
            for (int h = 0; h < hCount; h++)
            {   
                double rad = (double)h/(hCount-1)*r2 
                    + (1.0-(double)h/(hCount-1))*r1;

                // Divide coordinates by magnitude from center to yield unit 
                // circle position, scale with division by hole radius
                double x = vertices[i+h*nVPL][0];
                if (vertices[i+h*nVPL][0] != 0) 
                    vertices[i+h*nVPL][0] /= (mag/rad);
                if (vertices[i+h*nVPL][1] != 0) 
                    vertices[i+h*nVPL][1] /= (mag/rad);

                nodesShifted++;  // Increase the number of shifted nodes
                continue;
            }
        }
        // Calculating how many "hexes" away from the hole border
        double rkDist = (floor(((mag-r)-hexWidth)/hexWidth)+2)*hexWidth;
        if (rkDist < 0) rkDist = 0; // Hard limit to zero

        // Map values from the range hole radius to border hexes to range 0-1
        double xMap = ((std::abs(mag))-r)/(MAX_X-r);
        double yMap = ((std::abs(mag))-r)/(MAX_Y-r); 
        
        // Hard limit to zero   
        if (xMap < 0) xMap = 0; 
        if (yMap < 0) yMap = 0;   

        // Hard limit to one   
        if (xMap >= 1) continue; 
        if (yMap >= 1) continue;

        double randomX = 0; double randomY = 0; // Initialize changes

        // Never change border values; change nothing if randomness is zero
        if (maxRand > 0 && xMap != 0 && yMap != 0) 
        {
            // Generate random values between -maxRand and +maxRand
            randomX = (rand() % (2*maxRand) - maxRand)/divisor; 
            randomY = (rand() % (2*maxRand) - maxRand)/divisor; 
        }

        double newX = x; double newY = y; // Initialize new coordinates
        if (x != 0) newX = (x*xMap) + x/ (mag)*(r+rkDist) * (1-xMap);
        if (y != 0) newY = (y*yMap) + y/ (mag)*(r+rkDist) * (1-yMap);

        for (int h = 0; h < hCount; h++) // For each layer
        {
            // Shift nodes according to balance
            vertices[i+xCount*yCount*h][0] = newX;
            vertices[i+xCount*yCount*h][1] = newY;

            // Shift nodes according to randomness if within borders
            if (std::abs(newX + randomX < MAX_X)) 
                vertices[i+xCount*yCount*h][0] += randomX;
            if (std::abs(newY + randomY < MAX_Y)) 
                vertices[i+xCount*yCount*h][1] += randomY;

            nodesShifted++; // Increase the number of shifted nodes
        }
    }
    c->p(E + "Nodes round-shifted: " + to_string(nodesShifted));

    int iteration = 0; // Convexity iteration lap number
    int fullLoop = 0; // Full lattice iteration lap number
    bool allConvex = true; // Assume all quads in the lattice are convex
    do
    {
        fullLoop++; // Increase lap number
        allConvex = true; // Reset full convexity assumption
        for (int i = 0; i < nVert/hCount; i++) // For each vertex in layer
        {
            // Do not go too far
            if (i >= nVert/hCount-xCount || i % xCount >= yCount-1) continue;

            // Indices of the vertices in the quad
            int indices[4] = {i, i+1, i+xCount+1, i+xCount};

            double quad[4][2]; // Solve the quad
            for (int d = 0; d < 2; d++) // For each dimension (X and Y)
            {
                for (int j = 0; j < 4; j++) // For each point
                    quad[j][d] = vertices[indices[j]][d];
            }

            bool convexities[4] = {true}; // Signs of the cps of the corners

            while (!isConvex(quad, convexities))
            {
                iteration++; // Increase lap number
                int iCC = 0; // Index of concave corner
                int nCC = 0; // Number of "concave" corners

                // Find which corner is concave
                for (int b = 0; b < 4; b++) // For each sign
                {
                    if (!convexities[b]) // Concave
                    {
                        iCC = b; // Index of concave found
                        nCC++; // Increase the number of "concaves" found
                    }
                }
                if (nCC != 1) break; // Not actually concave (probably hole)
                allConvex = false; // Not all quads in the lattice are convex

                int cV = indices[iCC]; // Concave vertex
                int oV = indices[(iCC+2)%4]; // Opposite vertex

                // Calculate the magnitudes of opposing corners
                double x = vertices[cV][0]; 
                double y = vertices[cV][1];    
                double magV = std::sqrt(x*x+y*y);
                x = vertices[oV][0];
                y = vertices[oV][1];
                double magO = std::sqrt(x*x+y*y);

                double cFMP = 0.001; // Convex fix multiplier

                // Pull inwards inner concave corners, push out outer
                if (magV < magO) cFMP *= -1; 
                vertices[cV][0] *= (1.0 + cFMP);
                vertices[cV][1] *= (1.0 + cFMP);            

                // Reset the quad
                for (int d = 0; d < 2; d++) // For each dimension (X and Y)
                {
                    for (int j = 0; j < 4; j++) // For each point
                    {
                        quad[j][d] = vertices[indices[j]][d]; // New coordinates
                    }
                }
            }

            // Change the vertex positions in all layers accordingly
            for (int i = 0; i < nVert/hCount; i++) // For each point on layer
            {
                for (int h = 1; h < hCount; h++) // For each layer
                {
                    vertices[i+xCount*yCount*h][0] = vertices[i][0];
                    vertices[i+xCount*yCount*h][1] = vertices[i][1];
                }
            }
        }
    } while (!allConvex);    
    
    c->p(E + "Concave quads eliminated through " + to_string(iteration) 
        + " iterations during " + to_string(fullLoop) + " loops");
}