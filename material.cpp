#include "material.hpp"

/** Kalvo Material Data Retriever
 * 
 * @author Panu Lappalainen
 * @version 200410
 *  
 * @param cp pointer to the CoolPrint class
 */
Material::Material(CoolPrint* cp)
{
    c = cp;
} 

/** Sets all class attributes as the properties of given material
 * 
 * @param material whose properties become the class attributes
 */
void Material::setAllProperties(string material)
{
    lambda = getProperty(material, "lambda");
    mu = getProperty(material, "mu");
    rho = getProperty(material, "rho");
}

/** Gets the value of a property for the material from the material 
 *  parameter file. May terminate the program with error code 2.
 * 
 * @param material given material
 * @param property desired property to be found
 * @return the value of the property if found, else 0.0
 */
double Material::getProperty(string material, string property)
{
    string str; // Current line
    bool atMaterial = false; // Lines are of the correct material
    std::ifstream file(materialParams); // Initialize file stream
    while (std::getline(file, str)) // For each line
    {
        // Skips #commented lines 
        if (str[0] == '#') continue;

        // Find header line 
        if (str[0] != ' ')
        {
            // Header line after correct header line has been found means all
            // properties have been browsed through for the material 
            if (atMaterial)
            {
                c->p(string("property ") + property + " not found for " + 
                    material + " in " + materialParams, warnings);
                terminateAbruptly(Settings::matPropNotFound);
                return 0.0;
            }
            // At correct header line 
            if (str == material + ":")
            {
                atMaterial = true;
                continue;
            }
        }
        if (!atMaterial) continue;

        // Strip the beginning whitespace 
        while (str[0] == ' ') str = str.substr(1);
        
        // At correct property 
        int startingIndex = str.find('=');
        if (str.substr(0, startingIndex) == property)
        {
            string valueString = str.substr(startingIndex+1, 
                str.find(' ') - startingIndex);
            double value = stod(valueString);
            c->p(property + " of " + material + ": " + to_string(value));
            return value;
        }     
    }

    // Getting through the file with no matches means the given material
    // was not listed 
    c->p(string("material ") + material + " not found in " + materialParams, warnings);
    terminateAbruptly(Settings::matNotFound);
    return 0.0;
}

/** Terminates the entire program with non-zero exit code 
 * 
 * @param errorCode integer of the exit status
 *  4: material not found
 *  5: property not found
*/
void Material::terminateAbruptly(int errorCode)
{
    MPI_Finalize();
    exit(errorCode);
}