#include "settings.hpp"

/** Kalvo Command Line Argument Settings
 *
 * @author Panu Lappalainen
 * @version 210928
 *
 * @param argc number of arguments in argv
 * @param argv array of settings to be parsed and
 *  saved into class attributes
 * @param cp reference to the CoolPrint class
 */
Settings::Settings(int argc, char* argv[], CoolPrint& cp)
{
    c = &cp; // Set CoolPrint attribute
    myid = cp.getMyid(); // Set processor number

    // Go through the command line arguments
    for (int i = 1; i < argc; i++)
    {
        string setting(argv[i]);

        // Clean the beginning dash
        if (setting[0] == '-') setting = setting.substr(1);

        // Check if "help" is wanted
        if (setting == "help" || setting == "-help")
        {
            showHelp();
            p = noprint;
            halt = HELP_HALT; // Halt without error code
            return;
        }

        // Otherwise change the setting
        if (!changeSetting(setting, false))
        {
            c->p(string("a parameter was given more than one value"), warnings);
            halt = paramMoreThan1;
            return;
        }
        if (halt != 0) return;
    }

    // Go through the default settings
    string str;
    std::ifstream file(defaultSettings);
    while (std::getline(file, str))
    {
        // Skips #commented lines
        if (str.size() < 1) continue;
        if (str[0] == '#') continue;
        changeSetting(str, true);
    }

    // Apply filling factor
    if (F > 0) 
    {
        double secA = a;
        if (a2 > 0) secA = a2;
        r = sqrt(F*a*secA/M_PI);
        c->p(string("r set to ") + to_string(r) + " from filling factor", mods);
    }

    // Warning conditions
    if (r == 0 && S == 1)
    {
        c->p(string("zero radius with snapping enabled may cause uneven")
            + " geometry. S=0 recommended.", warnings);
    }

    // Halting conditions
    if (h.size() != m.size())
    {
        c->p(string("number of materials does not match with the number of heights given, ")
            + to_string(m.size()) + " vs " + to_string(h.size()), warnings);
        halt = matHeightMismatch;
    }
    if ((a <= 0 && e[0] <= 0) || (s <= 0 && e[0] <= 0)
        || (a <= 0 && s <= 0))
    {
        c->p(string("of a, e and s, two cannot be zero"), warnings);
        halt = tooManyUndefined;
    }
    if (h[0] <= 0 && s <= 0)
    {
        c->p(string("you must define either h or s"), warnings);
        halt = tooManyUndefined;
    }
    if ((a > 0 && r >= a/2) || (a2 > 0 && r >= a2/2) ||
        (e[0] > 0 && s > 0 && r >= e[0]*s/2))
    {
        c->p(string("the hole is too big for the lattice"), warnings);
        halt = tooBigHole;
    }
    if (getNumberOfMaterials() > e[2] && s == 0)
    {
        c->p(string("the number of vertical elements, ") + to_string(e[2]) +
            ", is fewer than the number of materials, " + 
            to_string(getNumberOfMaterials()), warnings);
        halt = matHeightMismatch;
    }
}

/** Reads the K file and sets the K vectors as class attributes */
void Settings::setKVectorsFromFile()
{
    if (kCount > 0) return;
    string str; // Single line of the file
    int localVectorCounter = 0; // For tracking the number of vectors
    bool vectorNumberFound = false; // Finding line with number of vectors in it
    std::ifstream file(K); // Initialize file
    while (std::getline(file, str))
    {
        // Skip #commented lines
        if (str[0] == '#') continue;
        int delimiterIndex = str.find(',');

        // First find the number of vectors
        if (!vectorNumberFound)
        {
            if (delimiterIndex == (int) string::npos)
            {
                kCount = stod(str);
                vectorNumberFound = true;
                c->p("Number of K vectors: " + to_string(kCount), mods);
                continue;
            }
            continue;
        }

        // Separate kx and ky from the string into their vectors
        string x = str.substr(0,delimiterIndex);
        string y = str.substr(delimiterIndex+2);
        if (y[0] == ' ') y = y.substr(1);
        localVectorCounter++;
        c->p(to_string(localVectorCounter) + ": (" + x + ", " + y + ")");
        kx.push_back(stod(x));
        ky.push_back(stod(y));
    }

    // No vectors found
    if (localVectorCounter == 0)
    {
        c->p(string("Failed to find any K vectors in file ") +  K, warnings);
        halt = kFileError;
        return;
    }

    // Warn when the counted vectors differs from the attribute
    if (localVectorCounter != kCount)
    {
        c->p(string("local vector count does not match with the one given in ")
            + K + ", " + to_string(localVectorCounter) + " vs " +
            to_string(kCount), warnings);
        halt = vectorMismatch;
    }
}

/** Interprets the argument string and sets the K vectors as class attributes
 *  TODO: Add X2/Y (for non-square base)
 *
 * @param shape point-to-point path of K-values (eg. GXMG)
 *     M
 *    /|
 *   / |
 *  G--X
 * @param kNumber number of K-values
 */
void Settings::setKVectorsFromArgs(string shape, int kNumber)
{
    // Check for faulty input
    bool error = false;
    if (shape.length() < 2 || shape.length() > 4) error = true;
    for (int i = 0; i < shape.length(); i++)
    {
        // For backwards compability, 'K' also works instead of 'M'
        if (shape[i] != 'G' && shape[i] != 'K' && shape[i] != 'M'
            && shape[i] != 'X')
        {
            error = true;
            break;
        }
    }
    if (error)
    {
        c->p(string("the k-space path input \"" + shape +
            "\" does not correspond to any possible path along GXMG points"),
            warnings);
        halt = kPathError;
        return;
    }

    kCount = kNumber;

    bool GXM[3] = {false, false, false}; // G-, X- and M-points, respectively
    double sides[3] = {0.0, 0.0, 0.0}; // Lengths of the sides of the triangle
    if (shape.find("GX") != string::npos || shape.find("XG") != string::npos)
    {
        sides[0] = 1.0; // Catethus 1
        GXM[0] = GXM[1] = true;
    }
    if (shape.find("XK") != string::npos || shape.find("KX") != string::npos ||
        shape.find("XM") != string::npos || shape.find("MX") != string::npos)
    {
        sides[1] = 1.0; // Catethus 2
        GXM[1] = GXM[2] = true;
    }
    if (shape.find("KG") != string::npos || shape.find("GK") != string::npos ||
        shape.find("MG") != string::npos || shape.find("GM") != string::npos)
    {
        sides[2] = sqrt(2); // Hypotenuse
        GXM[2] = GXM[0] = true;
    }

    // Remove corner points from total count
    for (int i = 0; i < 3; i++) if (GXM[i]) kNumber--;

    double circumferance = sides[0] + sides[1] + sides[2];
    int pointsPerSide[3] = {0, 0, 0};
    int kLeft = kNumber;

    // Distribute the points (roughly evenly) among the sides
    for (int i = 0; i < 3; i++)
    {
        if (sides[i] > 0)
        {
            pointsPerSide[i] = round(kNumber*(sides[i]/circumferance));
            kLeft -= pointsPerSide[i];
        }
    }

    // Add potentially remaining points into hypotenuse
    pointsPerSide[1] += kLeft;

    c->p("points per sides: " + to_string(pointsPerSide[0]) + "," +
        to_string(pointsPerSide[1]) + "," + to_string(pointsPerSide[2]), all);

    // Corner points
    double xGXM[3] = {0.0, 1.0, 1.0};
    double yGXM[3] = {0.0, 0.0, 1.0};

    // Multipliers
    double x[3] = {1, -1, 1};
    double y[3] = {0, 1, 1};
    for (int s = 0; s < 3; s++)
    {
        if (GXM[s]) // Add the corner points
        {
            kx.push_back(xGXM[s]);
            ky.push_back(yGXM[s]);
        }
        if (sides[s] > 0) // Add the intermediate points
        {
            for (int i = 0; i < pointsPerSide[s]; i++)
            {
                double xV;
                if (s == 1) xV = 1; // X-M side has constant X (at max)
                else xV = x[s]*(i+1)/(pointsPerSide[s]+1);
                double yV = y[s]*(i+1)/(pointsPerSide[s]+1);
                if (s == 2) // M-G side direction should be inverted
                {
                    xV = 1 - xV;
                    yV = 1 - yV;
                }

                kx.push_back(xV);
                ky.push_back(yV);
            }
        }
    }

    // Multiply all values with pi
    for (int i = 0; i < kCount; i++)
    {
        kx[i] *= M_PI;
        ky[i] *= M_PI;
    }
}

/** Splits a string into a vector
 *
 * @param strToSplit string to be split
 * @param delimeter character to split by
 * @return vector consisting of the split string
 */
vector<string> Settings::split(string strToSplit, char delimiter)
{
     std::stringstream ss(strToSplit);
     string item;
     vector<string> splitStrings;
     while (std::getline(ss, item, delimiter))
     {
         splitStrings.push_back(item);
     }
     return splitStrings;
}

/** Changes the value of a setting and saves it in the
 *  corresponding attribute
 *
 * @param settingAndValue a block of the user input containing
 *  setting name and value in the format "setting=value"
 *  where "setting" consists of a single character
 * @param defaults if changing from the default settings
 * @return whether a setting was given one and only one value
 */
bool Settings::changeSetting(string settingAndValue, bool defaults)
{
    char setting = settingAndValue[0];
    string value = settingAndValue.substr(2);
    bool settingFound = true;
    string parsedValue = "ERROR";
    string rfError = string("r and F have conflicting values,")
        + " ignore by setting one of them as 0";

    switch (setting)
    {
        // Integers(/"booleans")
        case 'p': // Print level
            if (p >= defp)
            {
                int res = stod(value);
                p = res;
                c->setPrintLevel(res);
                parsedValue = to_string(res);
            }
            else return false;
            break;

        case 'b': // Boundary elements on/off
            if (b < 0)
            {
                int res = stod(value);
                b = res;
                parsedValue = to_string(res);
            }
            else return false;
            break;

        case 'v': // Visualization on/off
            if (v < 0)
            {
                int res = stod(value);
                v = res;
                parsedValue = to_string(res);
            }
            else return false;
            break;

        case 'S': // Snap nodes to cylinder shape on/off
            if (S < 0)
            {
                int res = stod(value);
                S = res;
                parsedValue = to_string(res);
            }
            else return false;
            break;

        case 'P': // Periodic borders on/off
            if (P < 0)
            {
                int res = stod(value);
                P = res;
                parsedValue = to_string(res);
            }
            else return false;
            break;

        case 'R': // Maximum lattice point random variation
            if (R < 0)
            {
                int res = stod(value);
                R = res;
                parsedValue = to_string(res);
            }
            else return false;
            break;

        case 'n': // Number of eigenfrequencies
            if (n <= 0)
            {
                int res = stod(value);
                n = res;
                parsedValue = to_string(res);
            }
            else return false;
            break;

        // Doubles
        case 'a': // Lattice constant
            if (a <= 0)
            {
                if (value.find(',') != std::string::npos)
                {
                    vector<string> res = split(value, ',');
                    a = stod(res[0])*SCALING_FACTOR;
                    a2 = stod(res[1])*SCALING_FACTOR;
                    parsedValue = to_string(a) + " and a2 to " + to_string(a2);
                }
                else
                {
                    double res = stod(value)*SCALING_FACTOR;
                    a = res;
                    parsedValue = to_string(res);
                }
            }
            else return false;
            break;

        case 'r': // Radius of the hole
            if (r < 0)
            {
                if (value.find(',') != std::string::npos)
                {
                    vector<string> res = split(value, ',');
                    r = stod(res[0])*SCALING_FACTOR;
                    r2 = stod(res[1])*SCALING_FACTOR;
                    parsedValue = to_string(r) + " and r2 to " + to_string(r2);
                }
                else
                {
                    double res = stod(value)*SCALING_FACTOR;
                    r = res;
                    r2 = res;
                    parsedValue = to_string(res);
                }
                if (r > 0 && F > 0)
                {
                    c->p(rfError, warnings);
                    halt = paramMoreThan1;
                    return true;
                }
            }
            else return false;
            break;

        case 'F': // Filling factor
            if (F < 0)
            {
                double res = stod(value);
                if (res > 0)
                {
                    if (r > 0)
                    {
                        c->p(rfError, warnings);
                        halt = paramMoreThan1;
                        return true;
                    }
                    F = res;
                    parsedValue = to_string(res);
                    
                }
                if (defaults && res == 0) 
                {
                    c->p("F not set", all);
                    return true;
                }
            }
            else return false;
            break;

        case 's': // Preferred size of a component hexahedron
            if (s <= 0)
            {
                double res = stod(value)*SCALING_FACTOR;
                s = res;
                parsedValue = to_string(res);
            }
            else return false;
            break;

        // Vectors
        case 'h': // Thicknesses (doubles)
            if (h.size() < 1)
            {
                vector<string> res = split(value, ',');
                const int LEN = res.size();
                for (int i = 0; i < LEN; i++)
                    h.push_back(stod(res[i])*SCALING_FACTOR);
                parsedValue = "";
                for (auto i = h.begin(); i != h.end(); ++i)
                    parsedValue += to_string(*i) +' ';
            }
            else return false;
            break;

        case 'm': // Materials (strings)
            if (m.size() < 1)
            {
                vector<string> res = split(value, ',');
                m = res;
                parsedValue = "";
                for (auto i = m.begin(); i != m.end(); ++i)
                    parsedValue += *i +' ';
            }
            else return false;
            break;

        case 'e': // Number of mesh elements per unitcell side (int)
            if (e.size() < 1)
            {
                // NxNxM lattice can be created with "N,,M"
                int doubleComma = value.find(",,");
                if (doubleComma > 0)
                {
                    string v1 = value.substr(0,doubleComma);
                    value.insert(doubleComma+1, v1);
                }

                vector<string> res = split(value, ',');
                if (res.size() < 3)
                {
                    c->p(string("size of element count (") 
                        + to_string(res.size()) + ") is below 3", warnings);
                    halt = tooFewElements;
                    return false;
                }
                for (int i = 0; i < 3; i++) e.push_back(stod(res[i]));
                parsedValue = "";
                for (auto i = e.begin(); i != e.end(); ++i)
                    parsedValue += to_string(*i) +' ';
            }
            else return false;
            break;

        // Strings
        case 'd': // Default setting file
            defaultSettings = value;
            parsedValue = defaultSettings;
            break;

        case 'K': // k-space sample file
            if (K == "")
            {
                c->p("Setting K to " + value, all);
                K = value;
                parsedValue = K;
                setKVectorsFromFile();
            }
            else return false;
            break;

        case 'k': // k-space sampling point path and rate
            {
                vector<string> res = split(value, ',');
                if (res.size() < 2)
                {
                    c->p(string("the k-space path/resolution input \"k=" + value +
                        "\" has too few parameters. (Maybe forgot rate?)"),
                        warnings);
                    halt = kPathError;
                    return true;
                }
                int nK = stod(res[1]);
                setKVectorsFromArgs(res[0], nK);
                c->p("Setting k to " + value, all);
                parsedValue = value;
            }
            break;

        // No matches
        default:
            settingFound = false;
            break;
    }

    // Print the success messages for settings changed through arguments
    string ifDefault = "";
    if (defaults) ifDefault = " from " + defaultSettings;
    if ((settingFound && p >= all) || (settingFound && !defaults))
        c->p(string(1, setting) + " set to " + parsedValue  + ifDefault, mods);
    else if (!settingFound)
    {
        c->p(settingAndValue + ifDefault + " not recognized as a valid input!",
            warnings);
        return true;
    }
    return settingFound;
}

/** Shows help, summoned with --help */
void Settings::showHelp()
{
    if (myid > 0) return;
    cout << endl << "Exit code definitions:" << endl
        << matNotFound << ": Material not found" << endl
        << matPropNotFound << ": Material property not found" << endl
        << matHeightMismatch << ": Mismatch in number of materials and" 
            << " number of heights or vertical elements " << endl
        << tooManyUndefined << ": Too many undefined parameters" << endl
        << tooBigHole << ": Too big hole for the lattice " << endl
        << vectorMismatch << ": Mismatch in number of vectors in the file and "
            << "number given on the first line of the file" << endl
        << tooFewElements << ": Too few numbers of component elements " << endl
        << kFileError << ": Error in k-space sample file " << endl
        << kPathError << ": Error in k-space path or sampling resolution " << endl
        << paramMoreThan1 << ": One of the parameters has been defined more " 
            << "than once " << endl
        << endl;

    cout << "Current default settings: " << endl;

    // Show the default parameters from the file
    std::ifstream file(defaultSettings);
    std::string str;
    while (std::getline(file, str)) cout << str << endl;

    cout << endl << "All options (case sensitive): " << endl;
    cout << "a, b, d, e, F, h, K, k, m, n, P, p, R, r, S, s, v" << endl << endl;
    cout << "Type your parameters in format \"a=100\" or \"-a=100\". " <<
        "Scroll up to see error codes definitions." << endl;
}
