/\\\--------/\\\---------------/\\\\\--------------------------------
\/\\\-----/\\\//---------------\///\\\-------------------------------
-\/\\\--/\\\//------/\\\\\\\\\----\/\\\------------------------------
--\/\\\\\\//\\\-----\////////\\\---\/\\\--/\\\----/\\\---------------
---\/\\\//-\//\\\-------------\\\---\/\\\-\//\\\--/\\\--\//////\-----
----\/\\\----\//\\\----/\\\\\\\\\\---\/\\\--\//\\\/\\\-\/     \///\--
-----\/\\\-----\//\\\--/\\\/////\\\---\/\\\---\//\\\\\--\        \/\-
------\/\\\------\/\\\-\//\\\\\\\-/\\-/\\\\\\\--\//\\\---\__      \/\
-------\///-------\///---\///////-\//-\///////----\///------\______\-
---------------------------------------------------------------------


Kalvo is a lightweight software for simulating dispersive properties
of single- or multilayered phononic crystal membranes using the
finite element method (FEM). It utilizes MFEM and SLEPc libraries and
can be run entirely with command prompts or Bash scripts. The
eigenfrequencies of a membrane in specified points in k-space are
solved and saved into a text file. Kalvo is being developed in the
Nanoscience Center and the Department of Physics in the University of
Jyväskylä.


Example prompts:
mpirun -n 8 Kalvo a=200 h=50 r=65 m=Si3N4 e=8,,2 k=GXMG,50
    mpirun    launches a job in Open Run-Time Environment (ORTE)
    -n 8      sets the number of parallel processes as 8
    (Kalvo    name of the job launched)
    a=200     sets the lattice constant as 200 nm
    h=50      sets the thickness of the membrane as 50 nm
    r=65      sets the radius of the hole as 65 nm
    m=Si3N4   sets silicon nitride as the material (with material
              paremeters from materialParameters.txt text file)
    e=8,,2    sets the number of hexahedral mesh elements as 8x8x2
              (equilevant to "e=8,8,2")
    k=GXMG,50 sets the k-space path in the irreducible Brillouin zone
              (IBZ) to to go from Gamma to X to M and back to Gamma,
              with a sampling resolution of 50


Version 20210928 (initial release) features:
- Single- or multilayered materials
- Square grid lattice
- Cylindrical hole
- GLVis visualization (experimental)
