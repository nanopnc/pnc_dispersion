#include "cleeps.hpp"

/** Classical linear elastic eigenvalue problem solver
 *  class constructor
 *  Adapted from MFEM example 12p
 *
 * @author Tuomas Puurtinen
 * @author Panu Lappalainen
 * @version 211005
 *
 * @param mesh pointer to the mesh
 * @param settings reference to the Settings class containing parsed and
 *   default parameters
 * @param ev pointer to the 2D double array of eigenvalues
 * @param cp pointer to the CoolPrint class
 */
CLEEPS::CLEEPS(Mesh* mesh, Settings& settings, double * ev, CoolPrint* cp)
{
    int nev = settings.getNumberOfEigenfrequencies(); // Number of eigenvalues
    bool visualization = settings.getVisualization(); // GLVis visualization

    int order = 2; // Testing indicates that order=3 is ~6x slower
    int dim = mesh->Dimension(); // Dimension is 3
    int myid = cp->getMyid(); // ID of current process
    c = cp; // Initialize printing

    // Reading mesh from a file instead
    if (settings.getMeshSaved())
    {
        delete mesh;
        mesh = new Mesh("pnc.mesh", 0, 0);
    }

    // Initialize SLEPc. This internally initializes PETSc as well.
    const char *slepcrc_file = "";
    MFEMInitializeSlepc(NULL, NULL, slepcrc_file, NULL);

    // Define a parallel mesh by a partitioning of the serial mesh.
    ParMesh *pmesh = new ParMesh(MPI_COMM_WORLD, *mesh);

    // Define a parallel finite element space on the parallel mesh using vector
    // finite elements, i.e. dim copies of a scalar finite element space.
    FiniteElementCollection *fec;
    ParFiniteElementSpace *fespace;
    fec = new H1_FECollection(order, dim);
    fespace = new ParFiniteElementSpace(pmesh, fec, dim, Ordering::byVDIM);
    HYPRE_Int size = fespace->GlobalTrueVSize();
    c->p(E + "Number of unknowns: " + to_string(size), all);

    // Set up the parallel bilinear forms a(.,.) and m(.,.) on the finite
    // element space corresponding to the linear elasticity integrator with
    // piece-wise constants coefficient lambda, mu and rho, a simple mass matrix
    // needed on the right hand side of the generalized eigenvalue problem
    // below. After serial/parallel assembly we extract the corresponding
    // parallel matrices A and M.

    // Initialize lambdas, mus and rhos
    const int NOM = settings.getNumberOfMaterials();
    Vector lambda(NOM);
    Vector mu(NOM);
    Vector rho(NOM);

    // Set parsed values of lambdas and mus for all materials
    const double DIV = 100;
    {
        vector<double> lambdas = settings.getLambdas();
        vector<double> mus = settings.getMus();
        vector<double> rhos = settings.getRhos();
        vector<string> materialNames = settings.getListOfMaterials();
        for (int i = 0; i < NOM; i++)
        {
            lambda[i] = lambdas[i] * DIV;
            mu[i] = mus[i] * DIV;
            rho[i] = rhos[i];
            c->p(materialNames[i] + ": lambda: " + to_string(lambda[i])
                + ", mu: " + to_string(mu[i]) 
                + ", rho: " + to_string(rho[i]), mods);
        }
    }

    // Initialize piece-wise constant coefficients
    PWConstCoefficient lambda_func(lambda);
    PWConstCoefficient mu_func(mu);
    PWConstCoefficient rho_func(rho);

    // Initialize the eigenvalue file
    std::ofstream eigenfile (settings.getFilename());
    eigenfile << (nev) << endl;

    // Set maximum k and lattice constant
    const int K_COUNT = settings.getNumberOfKVectors();
    const double LATTICE_CONSTANT1 = settings.getLatticeWidth();
    const double LATTICE_CONSTANT2 = settings.getLatticeBreadth();

    // Calculate for each K value
    for (int k = 0; k < K_COUNT; k++)
    {
        double kx = settings.getKx()[k] / LATTICE_CONSTANT1;
        double ky = settings.getKy()[k] / LATTICE_CONSTANT1;
        eigenfile << kx << "," << ky << endl;
        c->p("(" + to_string(k+1) +  "/" + to_string(K_COUNT) + ") kx: "
            + to_string(kx) + ", ky: " + to_string(ky), mods);
        if (myid == 0 && settings.getPrintLevel() >= all)
            cout << "matrix ... " << flush;

        // Set the complex parallel linear form for matrix A
        ParSesquilinearForm *a = new ParSesquilinearForm(fespace,
            ComplexOperator::HERMITIAN);

        // Set integrator for the non-derivative part
      	a->AddDomainIntegrator(new BlochVectorMassIntegrator(lambda_func,
            mu_func, kx, ky), NULL);

        // Set integrator for the first derivatives
        a->AddDomainIntegrator(NULL, new BlochGradientIntegrator(lambda_func,
            mu_func, kx, ky));

        // Set integrator for the second derivatives
        a->AddDomainIntegrator(new ElasticityIntegrator(lambda_func,
            mu_func), NULL);

        a->Assemble();
        a->Finalize();

        // Set the complex parallel linear form for matrix M
        ParSesquilinearForm *m = new ParSesquilinearForm(fespace,
            ComplexOperator::HERMITIAN);
        m->AddDomainIntegrator(new VectorMassIntegrator(rho_func), NULL);

        m->Assemble();
        m->Finalize();

        // Create the matrices
        ComplexHypreParMatrix *A = a->ParallelAssemble();
        ComplexHypreParMatrix *M = m->ParallelAssemble();
        HypreParMatrix *systemsA(A->GetSystemMatrix());
        HypreParMatrix *systemsM(M->GetSystemMatrix());
        PetscParMatrix pA(systemsA);
        PetscParMatrix pM(systemsM);

        delete a;
        delete m;

        if (myid == 0 && settings.getPrintLevel() >= all) cout << "done." << endl;

        if (k == 0)
        {
            PetscInt nnz = pM.NNZ();
            c->p("Number of non-zeros in matrix A: " + to_string(nnz), mods);
        }

        // Set the matrices which define the generalized eigenproblem
        // A x = Lambda M x.
        SlepcEigenSolver *slepc = NULL;
        {
            slepc = new SlepcEigenSolver(MPI_COMM_WORLD);
            slepc->SetNumModes(nev*2);
            slepc->SetWhichEigenpairs(SlepcEigenSolver::TARGET_REAL);
            slepc->SetTarget(1.0);
            slepc->SetSpectralTransformation(SlepcEigenSolver::SHIFT_INVERT);
            slepc->SetOperators(pA,pM);
        }

        // Solve the eigenvalues
        Array<double> eigenvalues;
        slepc->Solve();
        eigenvalues.SetSize(nev*2);
        for (int i = 0; i < nev*2; i++)
        {
            slepc->GetEigenvalue(i,eigenvalues[i]);
        }

        // Create grid functions for visualization
        ParGridFunction xNC(fespace);
        ParComplexGridFunction x(fespace);

        // Set the eigenvalues in the local array
        const double GIGA = 1000000000;
        for (int i = 0; i < nev*2; i++)
        {
            double value = sqrt(1000/DIV)*sqrt(eigenvalues[i])/(2.0*M_PI)*GIGA;
            *(ev+k*nev+i) = value;
            if (i%2==1)
            {   c->p(to_string(value/GIGA));
                eigenfile << value << endl;
            }
        }

        if (visualization) // Visualizing the mesh with GLVis
        {
            // Send the above data by socket to a GLVis server
            char vishost[] = "localhost";
            int  visport   = 19916;
            socketstream mode_sock(vishost, visport);
            Vector temp(fespace->GetTrueVSize());

            // For each eigenvalue
            for (int i = 0; i < nev*2; i++)
            {
                if (myid == 0)
                {
                    cout << "Eigenmode " << i+1 << '/' << (nev*2)
                        << ", Lambda = " << eigenvalues[i] << endl;
                }

                // Convert eigenvector from HypreParVector to ParGridFunction
                slepc->GetEigenvector(i,temp);
                x.Distribute(temp);
                xNC = x.real();

                // Filename of the saved picture
                string picName = to_string(settings.getNumberOfElements()[0])
					+ "A" + to_string((int)settings.getLatticeWidth())
					+ "R" + to_string(settings.getRadius())
					+ "M" + to_string((i+1))
					+ ".png ";

                mode_sock << "parallel " << c->getNumProcs() << " " << myid
                    << "\n" << "solution\n" << *pmesh << xNC << flush
                    << "window_title 'Eigenmode " << i+1 << '/' << (nev*2)
                    << ", Lambda = " << eigenvalues[i] << "'" << endl;

                char ch;
                if (myid == 0)
                {
                    cout << "press (q)uit, (c)ontinue or (r)eset --> " << flush;
                    std::cin >> ch;
                }
                MPI_Bcast(&ch, 1, MPI_CHAR, 0, MPI_COMM_WORLD);

                if (ch == 'r') i = -1; // "r" resets the cycle to first value
                else if (ch != 'c') // "c" moves on to next eigenvalue
                {
                    break;
                }
            }
            mode_sock.close();
        }

        delete slepc;
        delete M;
        delete A;
    }
    eigenfile.close();

    if (fec)
    {
        delete fespace;
        delete fec;
    }
    delete pmesh;
}


/** Assembles the matrix for the given element fe
 *  that operates (M grad u, v.) 
 *
 * @param fe Single finite elemet
 * @param Trans Transformation information from mesh to reference element
 * @param elmat Local coefficient matrix
 */
void BlochGradientIntegrator::AssembleElementMatrix(const FiniteElement &fe,
    ElementTransformation &Trans, DenseMatrix &elmat)
{
    int dim = fe.GetDim(); // dim = 3
    int trial_dof = fe.GetDof();
    int test_dof = fe.GetDof(); // For this mesh/basis number of DoFs = 64
    Vector d_col1;

    dshape.SetSize(trial_dof, dim); // Matrix of derivatives of basis functions
    gshape.SetSize(trial_dof, dim); // Matrix of the scaled derivatives of BFs
    shape.SetSize(test_dof); // Values of BFs in DOFs (Vector)
    elmat.SetSize(dim * test_dof, dim * trial_dof);

    const IntegrationRule *ir = IntRule;
    if (ir == NULL)
    {
        int order = 1 * Trans.OrderGrad(&fe); // Integration order 1x is fine
        ir = &IntRules.Get(fe.GetGeomType(), order);
    }

    elmat = 0.0;
    elmat_comp1.SetSize(test_dof, trial_dof);
    elmat_comp2.SetSize(test_dof, trial_dof);

    const int nPoints = ir->GetNPoints();
    for (int i = 0; i < nPoints; i++)
    {
        const IntegrationPoint &ip = ir->IntPoint(i);

        fe.CalcDShape(ip, dshape);
        fe.CalcShape(ip, shape);

        Trans.SetIntPoint(&ip);
        Mult(dshape, Trans.AdjugateJacobian(), gshape);
        shape *= ip.weight;

        {
            lam = lambda->Eval(Trans, ip);
            mu = muc->Eval(Trans, ip);

            //MQxxx[test dim][trial dim][test derivative direction]
            MQpos[0][0][0] = (lam+2*mu)*kx;
            MQpos[0][0][1] = mu*ky;
            MQpos[0][0][2] = 0;
            MQpos[0][1][0] = lam*ky;
            MQpos[0][1][1] = mu*kx;
            MQpos[0][1][2] = 0;
            MQpos[0][2][0] = 0;
            MQpos[0][2][1] = 0;
            MQpos[0][2][2] = mu*kx;

            MQneg[0][0][0] = -(lam+2*mu)*kx;
            MQneg[0][0][1] = -mu*ky;
            MQneg[0][0][2] = 0;
            MQneg[0][1][0] = -mu*ky;
            MQneg[0][1][1] = -lam*kx;
            MQneg[0][1][2] = 0;
            MQneg[0][2][0] = 0;
            MQneg[0][2][1] = 0;
            MQneg[0][2][2] = -lam*kx;

            MQpos[1][0][0] = mu*ky;
            MQpos[1][0][1] = lam*kx;
            MQpos[1][0][2] = 0;
            MQpos[1][1][0] = mu*kx;
            MQpos[1][1][1] = (lam+2*mu)*ky;
            MQpos[1][1][2] = 0;
            MQpos[1][2][0] = 0;
            MQpos[1][2][1] = 0;
            MQpos[1][2][2] = mu*ky;

            MQneg[1][0][0] = -lam*ky;
            MQneg[1][0][1] = -mu*kx;
            MQneg[1][0][2] = 0;
            MQneg[1][1][0] = -mu*kx;
            MQneg[1][1][1] = -(lam+2*mu)*ky;
            MQneg[1][1][2] = 0;
            MQneg[1][2][0] = 0;
            MQneg[1][2][1] = 0;
            MQneg[1][2][2] = -lam*ky;

            MQpos[2][0][0] = 0;
            MQpos[2][0][1] = 0;
            MQpos[2][0][2] = lam*kx;
            MQpos[2][1][0] = 0;
            MQpos[2][1][1] = 0;
            MQpos[2][1][2] = lam*ky;
            MQpos[2][2][0] = mu*kx;
            MQpos[2][2][1] = mu*ky;
            MQpos[2][2][2] = 0;

            MQneg[2][0][0] = 0;
            MQneg[2][0][1] = 0;
            MQneg[2][0][2] = -mu*kx;
            MQneg[2][1][0] = 0;
            MQneg[2][1][1] = 0;
            MQneg[2][1][2] = -mu*ky;
            MQneg[2][2][0] = -mu*kx;
            MQneg[2][2][1] = -mu*ky;
            MQneg[2][2][2] = 0;
        }

        // Going through the derivatives of different directions
        for (int ddir = 0; ddir < dim; ddir++)
        {
            gshape.GetColumnReference(ddir, d_col1);
            MultVWt(shape, d_col1, elmat_comp1);
            MultVWt(d_col1, shape, elmat_comp2);

            // Going through v1,v2,v3 (test dimension)
            for (int vdim = 0; vdim < dim; vdim++)
            {
                // Going through u1,u2,u3 (trial dimension)
                for (int udim = 0; udim < dim; udim++)
                {
                    // Coef, Matrix, Row, Col
                    elmat.AddMatrix(MQpos[vdim][udim][ddir], elmat_comp1,
                        udim*trial_dof, vdim*test_dof);
                    elmat.AddMatrix(MQneg[vdim][udim][ddir], elmat_comp2,
                        udim*trial_dof, vdim*test_dof);
                }
            }
        }
    }
}

/** Assembles the matrix for the given element fe
 *  that operates (M u., v.) 
 *
 * @param fe Single finite elemet
 * @param Trans Transformation information from mesh to reference element
 * @param elmat Local coefficient matrix
 */
void BlochVectorMassIntegrator::AssembleElementMatrix(const FiniteElement &el,
    ElementTransformation &Trans, DenseMatrix &elmat)
{
    int nd = el.GetDof();
    int spaceDim = Trans.GetSpaceDim();

    double norm;

    // If vdim is not set, set it to the space dimension
    vdim = (vdim == -1) ? spaceDim : vdim;

    elmat.SetSize(nd*vdim);
    shape.SetSize(nd);
    partelmat.SetSize(nd);
    mcoeff.SetSize(vdim);

    const IntegrationRule *ir = IntRule;
    if (ir == NULL)
    {
        int order = 2 * el.GetOrder() + Trans.OrderW(); //2
        ir = &IntRules.Get(el.GetGeomType(), order);
    }

    elmat = 0.0;
    for (int s = 0; s < ir->GetNPoints(); s++)
    {
        const IntegrationPoint &ip = ir->IntPoint(s);
        el.CalcShape(ip, shape);

        Trans.SetIntPoint (&ip);
        norm = ip.weight * Trans.Weight();

        MultVVt(shape, partelmat);

        {
            double lam = lambda->Eval(Trans, ip);
            double mu = muc->Eval(Trans, ip);

            // Coefficient matrix for VectorMassIntegrator
            DenseMatrix vmidm(3);
            {
                vmidm(0,0) = (lam+2*mu)*kx*kx + mu*ky*ky;
                vmidm(0,1) = (lam+mu)*kx*ky;
                vmidm(0,2) = 0;

                vmidm(1,0) = (lam+mu)*kx*ky;
                vmidm(1,1) = (lam+2*mu)*ky*ky + mu*kx*kx;
                vmidm(1,2) = 0;

                vmidm(2,0) = 0;
                vmidm(2,1) = 0;
                vmidm(2,2) = mu*(kx*kx+ky*ky);
            }

            // Create a MatrixFunctionCoefficient from the DenseMatrix
            ConstantCoefficient coef(1.0);
            MatrixFunctionCoefficient MQ(vmidm, coef);

            MQ.Eval(mcoeff, Trans, ip);
            for (int i = 0; i < vdim; i++)
                for (int j = 0; j < vdim; j++)
                {
                    elmat.AddMatrix(norm*mcoeff(i,j), partelmat, nd*i, nd*j);
                }
        }
    }
}