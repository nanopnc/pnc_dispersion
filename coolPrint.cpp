#include "coolPrint.hpp"

/** Kalvo CoolPrint class constructor
 * 
 * @author Panu Lappalainen
 * @version 210510
 * 
 * @param printLevel determines the strength of console output
 * @param coreID number of the parallel core
 * @param np number of cores in use
 */
CoolPrint::CoolPrint(int printLevel, int coreID, int np)
{
    // Initialize class attributes
    pl = printLevel;
    myid = coreID;
    numProcs = np;
}

/** Cool print function (2/2 overloads)
 *  Function name abbreviated for easy-to-read use: "c.p()"
 * 
 * @param text to be printed in console
 * @param printLevel whether less or equal to the current setting 
 *  determines if the text is printed
 */
void CoolPrint::p(string text, int printLevel)
{
    // If processor 1 and sufficient print level
    if (myid == 0 && pl >= printLevel)
    {
        // For warnings, add warning message and formatting
        if (printLevel == warnings) text = "\033[1;31mWarning: \033[0m" + text;
        
        cout << text << endl; // Print the text
    }
}

/** Cool print function (1/2 overloads)
 *  Function name abbreviated for easy-to-read use: "c.p()"
 * 
 * @param text to be printed in console with lowest priority (printLevel=all)
 */
void CoolPrint::p(string text)
{
    p(text, all);
}