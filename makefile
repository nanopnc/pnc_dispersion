# Kalvo makefile
#
# @author Panu Lappalainen
# @version 210518
#
# Assumes path structure with all libraries in the same FOLDER 

CXXFLAGS= ${INCLUDES}
CCx = mpicxx
PETSC=../../petsc-3.13.3/arch-linux-c-debug
SLEPC=../../slepc-3.13.3/arch-linux-c-debug
PETSC2=../../petsc-3.13.3/
SLEPC2=../../slepc-3.13.3/

MFEM_DIR ?= ../..
MFEM_BUILD_DIR ?= ../..
CONFIG_MK = $(MFEM_BUILD_DIR)/config/config.mk
-include $(CONFIG_MK)
CFLAGS = -std=c++11 -I.. -I../../hypre/src/hypre/include -I../../metis-5.1.0/include -L.. -lmfem -L../../hypre/src/hypre/lib -lHYPRE  -L../../metis-5.1.0/lib -lmetis -lrt -I$(PETSC)/include -I$(PETSC2)/include -L$(PETSC)/lib -L$(PETSC2)/lib -I$(SLEPC)/include -I$(SLEPC2)/include -L$(SLEPC)/lib -L$(SLEPC2)/lib

DEPS = settings.hpp meshFactory.hpp cleeps.hpp material.hpp coolPrint.hpp
OBJ = Kalvo.o settings.o meshFactory.o cleeps.o material.o coolPrint.o 

EIGENDIR=eigenvalues

%.o: %.cpp $(DEPS)
	$(CCx) -c -o $@ $< $(CFLAGS) ${SLEPC_EPS_LIB}

Kalvo: $(OBJ)
	$(CCx) -o $@ $^ $(CFLAGS) ${SLEPC_EPS_LIB}
	
	[ -d $(EIGENDIR) ] || mkdir -p $(EIGENDIR)

include ${SLEPC_DIR}/lib/slepc/conf/slepc_common