#pragma region initializations
#ifndef CLEEPS_HPP
#define CLEEPS_HPP
#include "settings.hpp"
#include "material.hpp"

#include "mfem.hpp"
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdarg.h>

#ifndef MFEM_USE_SLEPC
#error This program requires that MFEM is build with MFEM_USE_SLEPC=YES
#endif

using std::string;
using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::to_string;
using std::flush;
using std::ofstream;
using std::numeric_limits;
using namespace mfem;
#pragma endregion initializations

/** Classical linear elastic eigenvalue problem solver
 *  adapted from MFEM example 12p
 *
 * @author Panu Lappalainen
 * @author Tuomas Puurtinen
 * @version 210603
 */
class CLEEPS {

private:
    enum PrintLevels {noprint, warnings, mods, all}; // Print levels
    CoolPrint* c; // CoolPrint pointer for printing
    const string E = ""; // Empty string

public: /* Functions*/
    CLEEPS(Mesh* mesh, Settings& parser, double * ev, CoolPrint* cp);
};


/** Class for integrating the bilinear form a(u,v.) := (M grad u, v.) where M 
 *  is a matrix coefficient, and v. is a vector with components v._i in the 
 *  same space as u
 *
 * @author Panu Lappalainen
 * @author Tuomas Puurtinen
 * @version 210410
 */
class BlochGradientIntegrator : public BilinearFormIntegrator
{
protected:
    Coefficient *Q;

private:
    Vector shape;
    DenseMatrix dshape;
    DenseMatrix gshape;
    DenseMatrix elmat_comp1;
    DenseMatrix elmat_comp2;
    double MQpos[3][3][3];
    double MQneg[3][3][3];
    Vector pa_data;
    const DofToQuad *trial_maps, *test_maps; ///< Not owned
    const GeometricFactors *geom;            ///< Not owned
    int dim, ne, nq;
    int trial_dofs1D, test_dofs1D, quad1D;
    double lam, mu;
    double kx, ky;
    Coefficient *lambda, *muc;

public:
    BlochGradientIntegrator(Coefficient &l, Coefficient &m, double x, double y)
    {
        lambda = &l;
        muc = &m;
        kx = x;
        ky = y;
    }

    virtual void AssembleElementMatrix(const FiniteElement &fe,
        ElementTransformation &Trans, DenseMatrix &elmat);

    using BilinearFormIntegrator::AssemblePA;

    static const IntegrationRule &GetRule(const FiniteElement &trial_fe,
        const FiniteElement &test_fe, ElementTransformation &Trans);
};


/** Class for integrating the bilinear form a(u,v) := (M u., v.),
 *  where u.=(u1,...,un) and v.=(v1,...,vn); ui and vi are vectors
 *
 * @author Panu Lappalainen
 * @version 210410
 */
class BlochVectorMassIntegrator: public BilinearFormIntegrator
{
private:
    int vdim;
    Vector shape, te_shape, vec;
    DenseMatrix partelmat;
    DenseMatrix mcoeff;
    int Q_order;

protected:
    Vector pa_data;
    const DofToQuad *maps;         ///< Not owned
    const GeometricFactors *geom;  ///< Not owned
    int dim, ne, nq, dofs1D, quad1D;
    double kx, ky;
    Coefficient *lambda, *muc;

public:
    BlochVectorMassIntegrator(Coefficient &l, Coefficient &m, double kx,
        double ky) : lambda(&l), muc(&m), kx(kx), ky(ky), vdim(-1) {}

    int GetVDim() const { return vdim; }
    void SetVDim(int vdim) { this->vdim = vdim; }

    virtual void AssembleElementMatrix(const FiniteElement &el,
        ElementTransformation &Trans, DenseMatrix &elmat);

    static const IntegrationRule &GetRule(const FiniteElement &trial_fe,
        const FiniteElement &test_fe, ElementTransformation &Trans);
};

#endif // CLEEPS_HPP
