// IMPORTANT: Update the VERSION_NUMBER upon changing any of the classes
#define VERSION_NUMBER 20210928 /* YYYYMMDD */

#pragma region inclusions
#include "meshFactory.hpp"
#include "settings.hpp"

void readBinary(string filename);
int formTimeInteger();
string formMaterialString(vector<string> mats);
string formFilename(Settings& Settings, string materials, bool isBinary);
#pragma endregion inclusions

/** Kalvo main function
 *  execute parallel with "mpirun -n 4 Kalvo"
 *
 * @author Panu Lappalainen
 * @author Tuomas Puurtinen
 * @version (check VERSION_NUMBER)
 *
 * @param argc number of command line arguments
 * @param argv command line arguments
 */
int main(int argc, char* argv[])
{
    int num_procs = 1; // Initialize number of processors used for current run
    int myid = 0;  // Initialize the ID of this processor

    const bool USE_MPI = true; // FALSE for debugging with Valgrind
    if (USE_MPI) // Initialize MPI
    {
        MPI_Init(&argc, &argv); // Initialize MPI
        MPI_Comm_size(MPI_COMM_WORLD, &num_procs); // Set the number of procs
        MPI_Comm_rank(MPI_COMM_WORLD, &myid); // Set the processor ID
    }

    CoolPrint c(5, myid, num_procs); // Initialize printing
    Settings p(argc, argv, c); // Initialize settings for input parameters

    int errorLevel = p.getHalt();
    if (errorLevel != 0) // Terminate the program for some reason
    {
        if (p.getPrintLevel() > 0) cout <<
            "\033[1;31mEnd condition reached.\033[0m Terminating run on core "
                << (1+myid) << " with exit code " << errorLevel << endl;
        if (USE_MPI) MPI_Finalize();
        if (errorLevel != p.HELP_HALT) 
            c.p(string("Type --help for exit code definitions"), 0);
        if (errorLevel < 0) errorLevel = 0; // Non-error halt
        return errorLevel;
    }

    p.setFilename(formFilename(p, formMaterialString(p.getListOfMaterials()), 
        false));

    // Creating the mesh and solving the eigenvalues
    { 
        MeshFactory mf(p); // Initialize MF class
        Mesh* mesh = mf.getMesh(); // MF owns the mesh and deletes it

        // Empty array for eigenvalues
        double eigenvalues[p.getNumberOfKVectors()]
            [p.getNumberOfEigenfrequencies()*2]; 

        CLEEPS(mesh, p, (double*)eigenvalues, &c); // Solve the eigenvalues
    }

    if (USE_MPI)
    {
      MFEMFinalizeSlepc();
      MPI_Finalize();
    }
    return 0;
}

/** (Deprecated) Forms an integer of the current time in format MMDDhhmmss
 *  where M = month of the year, D = day of month, h = hour of the day,
 *  m = minute of the hour and s = second of the minute. Leading
 *  zeroes are not shown for months (before november).
 *
 * @returns current date and time as integer
 */
int formTimeInteger()
{
    // Current time
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    const double T = 10.0; // Ten

    // Add each 2-digit time piece multiplied by ten to the power of its
    // position from the right multiplied by 2 (number of digits)
    int date = (timePtr->tm_mon)*pow(T,8)  +
        timePtr->tm_mday*pow(T,6) + timePtr->tm_hour*pow(T,4)
        + timePtr->tm_min*pow(T,2) + timePtr->tm_sec;
    return date;
}

/** Forms a single string of the names of the materials given
 *
 * @param mats list of material names
 * @returns string of material names separated by comma
 */
string formMaterialString(vector<string> mats)
{
    string materials = "";
    const int LEN = (int) mats.size();
    for (int i = 0; i < LEN; i++)
    {
        materials+= mats[i];
        if (i < LEN -1) materials += ",";
    }
    return materials;
}

/** Forms the filename
 *
 * @param settings reference to the Settings class containing handled properties
 * @param materials string of the material names
 * @param isBinary
 * @returns filename as string
 */
string formFilename(Settings& settings, string materials, bool isBinary)
{
    string filename = "";
    if (isBinary) filename += "bins/";
    else filename += "eigenvalues/"; // BUG: does not work if folder doesn't exist
    filename += materials;
    filename += "_a" + to_string(
        (int)(settings.getLatticeWidth()/settings.SCALING_FACTOR));
    filename += "_h";

    vector<double> heights = settings.getThicknesses();
    const int LEN = (int) heights.size();
    for (int i = 0; i < LEN; i++)
    {
        filename += to_string((int)(heights[i]/settings.SCALING_FACTOR));
        if (i < LEN-1) filename += ",";
    }
    filename += "_r" + to_string(
        (int)(settings.getRadius()/settings.SCALING_FACTOR));

    if (isBinary) filename += ".bin";
    else filename += ".txt";

    return filename;
}

/** Reads the given binary file and prints its contents in console
 *
 * @param filename path of the file to be read
 */
void readBinary(string filename)
{
    std::ifstream in(filename,ios_base::binary);
    // Version
    int version;
    in.read( reinterpret_cast<char *>(&version),sizeof(int) );
    cout << version << endl;

    // Size of material names
    unsigned matSizeRead;
    in.read( reinterpret_cast<char *>(&matSizeRead),sizeof(unsigned) );
    cout << matSizeRead << endl;

    // Material names
    vector<char> temp(matSizeRead);
    in.read( reinterpret_cast<char *>(&temp[0]),
        matSizeRead*sizeof(char) );
    string materialsRead(temp.begin(),temp.end());
    cout << materialsRead << endl;

    // Thickness
    double h;
    in.read( reinterpret_cast<char *>(&h),sizeof(double) );
    cout << h << endl;

    // Lattice constant
    double a;
    in.read( reinterpret_cast<char *>(&a),sizeof(double) );
    cout << a << endl;

    // Radius
    double r;
    in.read( reinterpret_cast<char *>(&r),sizeof(double) );
    cout << r << endl;

    // Number of k
    int kCount;
    in.read( reinterpret_cast<char *>(&kCount),sizeof(int) );
    cout << kCount << endl;

    // Number of eigenvalues
    int nev;
    in.read( reinterpret_cast<char *>(&nev),sizeof(int) );
    cout << nev << endl;

    double eigenRead[kCount][nev];
    for (int k = 0; k < kCount; k++) // For each K vector
    {
        // Kx and Ky
        double kx;
        in.read( reinterpret_cast<char *>(&kx),sizeof(double) );
        cout << kx << endl;
        double ky;
        in.read( reinterpret_cast<char *>(&ky),sizeof(double) );
        cout << ky << endl;

        // Eigenvalues
        in.read( reinterpret_cast<char *>(&eigenRead[k]),
            nev*sizeof(double) );
        for (int n = 0; n < nev; n++)
            cout << eigenRead[k][n] << " ";
        cout << endl;
    }

    in.close();
}
