#pragma region initializations
#ifndef MESHFACTORY_HPP
#define MESHFACTORY_HPP
#include "settings.hpp"
#include "cleeps.hpp"

#include "mfem.hpp"
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

using namespace mfem;
using std::string;
using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::to_string;
using std::flush;
using std::ofstream;

#pragma endregion initializations

/** Kalvo Mesh Generator
 *  Header file contains settings as class 
 *  attributes.
 * 
 * @author Panu Lappalainen
 * @version 210624
 */
class MeshFactory {
// -z       NOTATION:
//  | +y    x and y are the
//  | /     horizontal coordinates  
//  |/      whilst z (or h) is the
//  0---+x  vertical growing down

private: /* Private properties */
    enum PrintLevels {noprint, warnings, mods, all}; // Print levels
    int p = all; // Print level modifier

    double r = 0; // Average radius of the hole

    double xWidth = -1; // Width of the lattice
    double yBreadth = -1; // Breadth of the lattice
    double thickness = 0; // Height of the entire lattice
    vector<double> heights; // Heights of the individual material layers

    Mesh* mesh; 
    L2_FECollection* fec;
    FiniteElementSpace* fes;

    int order = 2; // FE order
    bool visualization = 1; // GLVis visualization
    bool makeBoundaries = true; // Boundary elements
    bool snapping = false; // Node snapping
    bool periodic = true; // Mesh edges fuse to opposing edges

    int maxRand = 0; // Maximum randomness
    double preferredHexSize = 10; // Target size of a component hexahedron

    int xCount = 0; // Number of vertices in X-direction (1st horizontal)
    int yCount = 0; // Number of vertices in Y-direction (2nd horizontal)
    int hCount = 0; // Number of vertices in Z-direction (vertical)

    double r1 = 0; // Minimum radius of the hole
    double r2 = 0; // Maximum radius of the hole

    double hexWidth = 0; // Length of the hexagon side in X-direction
    double hexBreadth = 0; // Length of the hexagon side in Z-direction
    double hexHeight = 0; // Length of the hexagon side in Y-direction

    int nVert = 0; // Number of vertices
    int nElem = 0; // Number of hexahedrons
    int nHorQuad = 0; // Number of horizontal boundary quads
    int nBorderQuad = 0; // Number of border boundary quads
    int nIntQuad = 0; // Number of internal boundary quads
    vector<int> nMatElems; // Number of elements per layer of material

    CoolPrint* c; // CoolPrint pointer for printing
    const string E = ""; // Empty string
    int myid; // Core ID

public: /* Public functions */
    MeshFactory(Settings& parser);
    void printShape(bool * blueprint);
    virtual ~MeshFactory();
    virtual Mesh* getMesh() {return mesh;};

private: /* Private functions */
    void setAttributes(Settings& parser);
    double SolveHexSide(double& size, int& count, double defaultSize, 
        double preferredHexSize);
    int solveVertices(double vertices[][3]);
    int solveHexahedrons(int hexahedrons[][8], 
        int horizontalBoundaryQuads[][4], bool * blueprint);
    void solveInternalBoundaries(int intBoundaries[][4], int hexahedrons[][8],
        bool * blueprint);
    void solveSizes();
    void visualize(Mesh& mesh, GridFunction& x);
    void saveSolution(Mesh *mesh, GridFunction& x);
    void makeBoundaryQuads(Mesh& mesh, int horizontalBoundaryQuads[][4],
        int intBoundaries[][4], int previousAttribute);
    void initializeCounts();
    void cleanupMesh(Mesh& mesh);
    void makeMesh(Mesh& mesh, double vertices[][3], int hexahedrons[][8], 
        int horizontalBoundaryQuads[][4], int intBoundaries[][4]);
    void mapSnap(double vertices[][3]);
};
#endif // MESHFACTORY_HPP
