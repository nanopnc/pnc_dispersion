#pragma region inclusions
#ifndef MATERIAL_HPP
#define MATERIAL_HPP
#include "settings.hpp"
#include "coolPrint.hpp"

#include "mpi.h"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <sstream>
#include <iterator>

using std::string;
using std::cout;
using std::endl;
using std::stoi;
using std::vector;

#pragma endregion inclusions

/** Kalvo Material Data Retriever
 * 
 * @author Panu Lappalainen
 * @version 210430
 */
class Material {

private: /*Properties*/    
    enum PrintLevels {noprint, warnings, mods, all}; // Print levels
    CoolPrint* c; // CoolPrint pointer for printing

    string materialParams = "materialParameters.txt"; // Properties file path
    string knownProperties[3] = {"lambda", "mu", "rho"}; // Lame & density names
    const int propertyCount = 3;

    double lambda = 0;
    double mu = 0;
    double rho = 0;

public: /* Functions */
    Material(CoolPrint* cp);
    double getProperty(string material, string property);
    void setAllProperties(string material);
    void terminateAbruptly(int errorCode);
    double getLambda() {return lambda;}
    double getMu() {return mu;}
    double getRho() {return rho;}
};
#endif // MATERIAL_HPP