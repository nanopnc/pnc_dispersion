#pragma region inclusions
#ifndef COOLPRINT_HPP
#define COOLPRINT_HPP
#include "coolPrint.hpp"
#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::stoi;
using std::to_string;
#pragma endregion inclusions

/** Kalvo Cool Printer
 *  Handles multicore printing on console during hyperthreading
 * 
 * @author Panu Lappalainen
 * @version 190719
 */
class CoolPrint {

private: /* Properties */
    enum PrintLevels {noprint, warnings, mods, all}; // Print levels
    int pl = all; // Print level modifier
    int myid = 0; // Core ID
    int numProcs = 0; // Number of processors

public: /* Functions */
    CoolPrint(int printLevel, int coreID, int np);
    void p(string text, int printLevel);
    void p(string text);

    int getMyid() {return myid;}
    int getNumProcs() {return numProcs;}
    void setMyid(int id) {myid = id;}
    void setPrintLevel(int p) {pl = p;}
};
#endif // COOLPRINT_HPP