#pragma region initializations
#ifndef PARSER_HPP
#define PARSER_HPP
#include "coolPrint.hpp"

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstring>
#include <sstream>
#include <iterator>
#include <ctime>
#include <math.h> 

using std::string;
using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::to_string;
using std::ios;
using std::ios_base;

#pragma endregion initializations

/** Kalvo Command Line Argument Settings
 *  Header file contains settings as class 
 *  attributes. For a full list with long names, 
 *  check the defaultSettings.txt file.
 * 
 * @author Panu Lappalainen
 * @version 211005
 */
class Settings {

public: /* Constants and enumerators */
    const double SCALING_FACTOR = 0.001; // Scale for nanometers
    const int HELP_HALT = -1;
    // Print levels
    enum PrintLevels {noprint, warnings, mods, all, defp};
    // Error codes
    enum ErrorCodes {allGood, generalCatchall, shellMisuse, unused, matNotFound, 
        matPropNotFound, matHeightMismatch, tooManyUndefined, tooBigHole,
        vectorMismatch, tooFewElements, kFileError, kPathError, paramMoreThan1};

private: /* Settings */

    int p = defp; // Print level modifier

    int b = -1; // Boundary boolean
    int v = -1; // Visualization boolean
    int S = -1; // Snapping boolean
    int P = -1; // Periodicity boolean
    int R = -1; // Randomness
    int n = -1; // Number of eigenfrequencies

    double a = -1; // Lattice constant/"width", length of X-component
    double a2 = -1; // Lattice constant/"breadth", length of Z-component
    double r = -1; // Primary radius of the hole
    double r2 = -1; // Secondary radius of the hole
    double F = -1; // Filling factor
    double s = -1; // Preferred size of a component hexahedron

    vector<double> h; // Thicknesses
    vector<string> m; // List of materials
    vector<int> e; // Numbers of mesh elements per side

    string K = ""; // k-space sample file path
    vector<double> lambdas; // List of lambdas
    vector<double> mus; // List of mus
    vector<double> rhos; // List of rhos

    int kCount = 0; // Number of k-vectors
    vector<double> kx; // k x-components
    vector<double> ky; // k y-components

    string defaultSettings = "defaultSettings.txt"; // Setting file path
    string filename = ""; // Text file name
    
    CoolPrint* c; // Printing handler
    int myid = 0; // Core ID
    int halt = 0; // Running level, !0 if stop, <0 if non-error

public: /* Getters */
    int getPrintLevel() {return p;}

    int getBoundaryBoolean() {return b;}
    int getVisualization() {return v;}
    int getSnapping() {return S;}
    int getPeriodicity() {return P;}
    int getRandomness() {return R;}
    int getNumberOfEigenfrequencies() {return n;}

    double getLatticeWidth() {return a;}
    double getLatticeBreadth() {return a2;}
    double getRadius() {return r;}
    double getRadius2() {return r2;}
    double getComponentHexPreferredSize() {return s;}

    vector<double> getThicknesses() {return h;}
    vector<string> getListOfMaterials() {return m;}
    int getNumberOfMaterials() {return (int) m.size();}
    vector<int> getNumberOfElements() {return e;}

    vector<double> getLambdas() {return lambdas;}
    vector<double> getMus() {return mus;}
    vector<double> getRhos() {return rhos;}

    int getNumberOfKVectors() {return kCount;}
    vector<double> getKx() {return kx;}
    vector<double> getKy() {return ky;}    

    bool getMeshSaved() {return false;} // Use old mesh from file pnc.mesh
    string getFilename() {return filename;}
    CoolPrint* getPrintingHandler() {return c;}
    int getHalt() {return halt;}

    void setFilename(string s) {filename = s;}
    void pushbackLambda(double d) {lambdas.push_back(d);}
    void pushbackMu(double d) {mus.push_back(d);}
    void pushbackRho(double d) {rhos.push_back(d);}

public: /* Public functions */
    Settings(int argc, char* argv[], CoolPrint& c);
    void showHelp();

private: /* Private functions */
    void setKVectorsFromFile();
    void setKVectorsFromArgs(string shape, int kNumber);
    bool changeSetting(string settingAndValue, bool defaults = false);
    vector<string> split(string strToSplit, char delimiter);
};
#endif // SETTINGS_HPP